<?php

return [
    'list resource' => 'List tags',
    'create resource' => 'Create tags',
    'edit resource' => 'Edit tags',
    'destroy resource' => 'Destroy tags',
    'tags' => 'Tagi',
    'tag' => 'Tag',
    'create tag' => 'Utwórz tag',
    'edit tag' => 'Edytuj tag',
    'name' => 'Nazwa',
    'slug' => 'Nazwa systemowa',
    'namespace' => 'Typ',
];
