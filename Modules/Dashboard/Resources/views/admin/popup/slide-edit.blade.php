@inject('entity', '\Modules\Slider\Entities\Slide')
@php $object = $entity->where(['file_id' => $data[0], 'slider_id' => $data[1]])->first() @endphp

<div class="modal modal-{{ $state }} fade in" id="slide-edit" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="delete-confirmation-title">{{ trans('slider::slider.popup.slide_content') }}</h4>
            </div>
            <div class="modal-body">
                <div class="nav-tabs-custom">
                    @include('dashboard::admin.popup.form-tab-headers')
                    <form action="{{ (!empty($object)) ? route('admin.slider.slide.update', $object->id) : route('admin.slider.slide.store') }}" id="slide-edit-form" method="POST">
                        @if(!empty($object))
                        {{ method_field('PUT') }}
                        @else
                        {{ method_field('POST') }}
                        @endif
                        <input type="hidden" name="slider_id" value="{{ $data[1] }}" />
                        <input type="hidden" name="file_id" value="{{ $data[0] }}" />
                        {{ csrf_field() }}
                        <div class="tab-content">
                            @foreach (LaravelLocalization::getSupportedLocales() as $locale => $language)
                            <div class="tab-pane {{ locale() == $locale ? 'active' : '' }}" id="popup_tab_{{ $loop->iteration }}">
                                <div class="box-body">
                                    <div class='form-group{{ $errors->has("{$locale}.title") ? ' has-error' : '' }}'>
                                        <input type="text" name="{{ $locale }}[title]" class="form-control" id="name" placeholder="{{ trans('slider::slider.popup.title') }}" value="{{ $object->{'title:'.$locale} or '' }}" />
                                        <input type="text" name="{{ $locale }}[content]" class="form-control" id="email" placeholder="{{ trans('slider::slider.popup.content') }}" value="{{ $object->{'content:'.$locale} or '' }}" />
                                        <input type="text" name="{{ $locale }}[body]" class="form-control" id="body" placeholder="{{ trans('slider::slider.popup.body') }}" value="{{ $object->{'body:'.$locale} or '' }}" />
                                        <input type="text" name="{{ $locale }}[url]" class="form-control" id="phone" placeholder="{{ trans('slider::slider.popup.url') }}" value="{{ $object->{'url:'.$locale} or '' }}" />
                                    </div>
                                </div>
                            </div>
                            @endforeach
                        </div>
                    </form>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-outline btn-flat" data-dismiss="modal">{{ trans('core::core.button.cancel') }}</button>
                <button type="submit" form="slide-edit-form" class="btn btn-outline btn-flat">{{ trans('core::core.button.update') }}</button>
            </div>
        </div>
    </div>
</div>