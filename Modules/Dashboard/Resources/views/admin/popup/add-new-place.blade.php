@inject('entity', '\Modules\Place\Entities\Place')
@inject('statusEntity', 'App\Status')


<div class="modal modal-{{ $state }} fade in" id="add-new-place" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="delete-confirmation-title">Add new place</h4>
            </div>
            <div class="modal-body" style="color: #000 !important;">
                <div class="box box-primary">
                    <div class="box-body">
                        {!! Form::open(['route' => ['admin.place.place.store'], 'method' => 'post']) !!}
                            <div class="form-group">
                                {!! Form::label("status", 'Status:') !!}
                                <select name="status" id="status" class="form-control">
                                    @foreach($statusEntity->lists() as $id => $status)
                                        <option value="{{ $id }}">{{ $status }}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class='hidden form-group{{ $errors->has('country') ? ' has-error' : '' }}'>
                                {!! Form::label('country', trans('place::place.form.country')) !!}
                                <input type="text" id="country" class="form-control" name="country" value="" />
                                {!! $errors->first('Country', '<span class="help-block">:message</span>') !!}
                            </div>

                            <div class='hidden form-group{{ $errors->has('province') ? ' has-error' : '' }}'>
                                {!! Form::label('province', trans('place::place.form.province')) !!}
                                <input type="text" id="province" class="form-control" name="province" value="" />
                                {!! $errors->first('Province', '<span class="help-block">:message</span>') !!}
                            </div>

                            <div class='form-group{{ $errors->has('city') ? ' has-error' : '' }}'>
                                {!! Form::label('city', trans('place::place.form.city')) !!}
                                <input type="text" id="city" class="form-control" name="city" value="" />
                                {!! $errors->first('City', '<span class="help-block">:message</span>') !!}
                            </div>

                            <div class='form-group{{ $errors->has('coordinates') ? ' has-error' : '' }}'>
                                {!! Form::label('coordinates', trans('place::place.form.coordinates')) !!}
                                <input type="text" id="coordinates" class="form-control" name="coordinates" value="" readonly />
                                {!! $errors->first('Coordinates', '<span class="help-block">:message</span>') !!}
                            </div>
                            <input type="hidden" id="place_id" class="form-control" name="gmap_place_id" value="" />

                            <div class='form-group{{ $errors->has('location') ? ' has-error' : '' }}'>
                                {!! Form::label('location', trans('place::place.form.location')) !!}
                                <input type="text" id="location" class="form-control" name="location" value="" />
                                {!! $errors->first('Location', '<span class="help-block">:message</span>') !!}
                            </div>

                            <div id="map" style="width: 100%; height: 500px;"></div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-outline btn-flat" data-dismiss="modal">{{ trans('core::core.button.cancel') }}</button>
                <button type="submit" form="slide-edit-form" class="btn btn-outline btn-flat">{{ trans('core::core.button.update') }}</button>
            </div>
        </div>
    </div>
</div>


<script type="text/javascript">
    $( document ).ready(function() {
            $('#map').mapify({
                zoom: 9,
                location: 'Podkarpackie, Rzeszów, Polska',
                title: 'Rzeszów',
                marker: false, // if you don't want to show marker on Google map
                onMapClick: true,
                autocomplete: 'location', // input id should be an autocomplete
                center: new google.maps.LatLng(50.0054088, 21.9184157)
            });
    });
    


//        var placeMap = new google.maps.Map(document.getElementById('map'), {
//          zoom: 9,
//          center: new google.maps.LatLng(50.0054088, 21.9184157)
//        });

</script>
