<?php

return [
    'fb' => [
        'description' => 'core::settings.fb',
        'view' => 'text',
        'translatable' => false,
    ],
    'twitter' => [
        'description' => 'core::settings.twitter',
        'view' => 'text',
        'translatable' => false,
    ],
 
];