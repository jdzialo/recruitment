<?php

namespace Modules\Category\Providers;

use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;
use Modules\Category\Events\Handlers\RootCategoryItemCreator;
use Modules\Category\Events\CategoryWasCreated;

class EventServiceProvider extends ServiceProvider
{
    protected $listen = [
        CategoryWasCreated::class => [
            RootCategoryItemCreator::class,
        ],
    ];
}
