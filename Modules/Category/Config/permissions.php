<?php

return [
    'category.categories' => [
        'index' => 'category::category.list resource',
        'create' => 'category::category.create resource',
        'edit' => 'category::category.edit resource',
        'destroy' => 'category::category.destroy resource',
    ],
    'category.categoryitems' => [
        'index' => 'category::category-items.list resource',
        'create' => 'category::category-items.create resource',
        'edit' => 'category::category-items.edit resource',
        'destroy' => 'category::category-items.destroy resource',
    ],
];
