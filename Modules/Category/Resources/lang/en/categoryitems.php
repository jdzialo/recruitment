<?php

return [
    'list resource' => 'List categoryitems',
    'create resource' => 'Create categoryitems',
    'edit resource' => 'Edit categoryitems',
    'destroy resource' => 'Destroy categoryitems',
    'title' => [
        'categoryitems' => 'Categoryitem',
        'create categoryitem' => 'Create a categoryitem',
        'edit categoryitem' => 'Edit a categoryitem',
    ],
    'button' => [
        'create categoryitem' => 'Create a categoryitem',
    ],
    'table' => [
    ],
    'form' => [
    ],
    'messages' => [
    ],
    'validation' => [
    ],
];
