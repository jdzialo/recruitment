<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCategoryCategoryitemEntityConnectionsTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('category__categoryitem_entity_connections', function(Blueprint $table) {
            $table->increments('id');
            $table->integer('categoryitem_id');
            $table->integer('entity_id');
            $table->string('entity_type'); //namespace

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::drop('category_categoryitem_entity_connections');
    }

}
