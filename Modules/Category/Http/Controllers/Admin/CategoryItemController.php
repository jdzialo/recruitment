<?php

namespace Modules\Category\Http\Controllers\Admin;

use Illuminate\Foundation\Http\FormRequest;
use Mcamara\LaravelLocalization\Facades\LaravelLocalization;
use Modules\Core\Http\Controllers\Admin\AdminBaseController;
use Modules\Category\Entities\Category;
use Modules\Category\Entities\Categoryitem;
use Modules\Category\Http\Requests\CreateCategoryItemRequest;
use Modules\Category\Http\Requests\UpdateCategoryItemRequest;
use Modules\Category\Repositories\CategoryItemRepository;
use Modules\Category\Services\CategoryItemUriGenerator;
use Modules\Page\Repositories\PageRepository;

class CategoryItemController extends AdminBaseController {

    /**
     * @var CategoryItemRepository
     */
    private $categoryItem;

    /**
     * @var PageRepository
     */
    private $page;

    /**
     * @var CategoryItemUriGenerator
     */
    private $categoryItemUriGenerator;

    public function __construct(CategoryItemRepository $categoryItem, PageRepository $page, CategoryItemUriGenerator $categoryItemUriGenerator) {
        parent::__construct();
        $this->categoryItem = $categoryItem;
        $this->page = $page;
        $this->categoryItemUriGenerator = $categoryItemUriGenerator;
    }

    public function create($category) {
        $category = Category::findOrFail($category);
        $pages = $this->page->all();

        $categorySelect = $this->getCategorySelect($category);

        return view('category::admin.categoryitems.create', compact('category', 'pages', 'categorySelect'));
    }

    public function store($category, CreateCategoryItemRequest $request) {
        $category = Category::findOrFail($category);
        $this->categoryItem->create($this->addCategoryId($category, $request));

        return redirect()->route('admin.category.category.edit', [$category->id])
                        ->withSuccess(trans('category::messages.categoryitem created'));
    }

    public function edit($category, $categoryItem) {
        $category = Category::findOrFail($category);
        $categoryItem = Categoryitem::findOrFail($categoryItem);
        $pages = $this->page->all();

        $categorySelect = $this->getCategorySelect($category);

        return view('category::admin.categoryitems.edit', compact('category', 'categoryItem', 'pages', 'categorySelect'));
    }

    public function update($category, $categoryItem, UpdateCategoryItemRequest $request) {
        $category = Category::findOrFail($category);
        $categoryItem = Categoryitem::findOrFail($categoryItem);
        
        $this->categoryItem->update($categoryItem, $this->addCategoryId($category, $request));

        return redirect()->route('admin.category.category.edit', [$category->id])
                        ->withSuccess(trans('category::messages.categoryitem updated'));
    }

    public function destroy(Category $category, Categoryitem $categoryItem) {
        $this->categoryItem->destroy($categoryItem);

        return redirect()->route('admin.category.category.edit', [$category->id])
                        ->withSuccess(trans('category::messages.categoryitem deleted'));
    }

    /**
     * @param Category, $categoryItemId
     * @return array
     */
    private function getCategorySelect($category) {
        return $category->categoryitems()->where('is_root', '!=', true)->get()->nest()->listsFlattened('title');
    }

    /**
     * @param  Category $category
     * @param  \Illuminate\Foundation\Http\FormRequest $request
     * @return array
     */
    private function addCategoryId(Category $category, FormRequest $request) {
        $data = $request->all();

        foreach (LaravelLocalization::getSupportedLanguagesKeys() as $lang) {
            if ($data['link_type'] === 'page' && !empty($data['page_id'])) {
                $data[$lang]['uri'] = $this->categoryItemUriGenerator->generateUri($data['page_id'], $data['parent_id'], $lang);
            }
        }

        if (empty($data['parent_id'])) {
            $data['parent_id'] = $this->categoryItem->getRootForCategory($category->id)->id;
        }

        return array_merge($data, ['category_id' => $category->id]);
    }

}
