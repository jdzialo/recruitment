<?php

use Illuminate\Routing\Router;

/** @var Router $router */
$router->group(['prefix' => '/categoryitem', 'middleware' => 'api.token'], function (Router $router) {
    $router->post('/update', [
        'as' => 'api.categoryitem.update',
        'uses' => 'CategoryItemController@update',
        'middleware' => 'token-can:category.categoryitems.edit',
    ]);
    $router->post('/delete', [
        'as' => 'api.categoryitem.delete',
        'uses' => 'CategoryItemController@delete',
        'middleware' => 'token-can:category.categoryitems.destroy',
    ]);
});
