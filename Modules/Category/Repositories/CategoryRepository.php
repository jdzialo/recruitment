<?php

namespace Modules\Category\Repositories;

use Modules\Core\Repositories\BaseRepository;

interface CategoryRepository extends BaseRepository
{
    /**
     * Get all online menus
     * @return object
     */
    public function allOnline();
}
