<?php

return [
    'list resource' => 'List sliders',
    'create resource' => 'Create sliders',
    'edit resource' => 'Edit sliders',
    'destroy resource' => 'Destroy sliders',
    'title' => [
        'sliders' => 'Sliders',
        'create slider' => 'Create a slider',
        'edit slider' => 'Edit a slider',
    ],
    'button' => [
        'create slider' => 'Create a slider',
    ],
    'table' => [
    ],
    'form' => [
      'title' => 'title',
    ],
    'messages' => [
    ],
    'validation' => [
    ],
];
