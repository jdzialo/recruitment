<?php

namespace Modules\Slider\Entities;

use Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;
use Modules\Core\Traits\NamespacedEntity;
use Modules\Media\Support\Traits\MediaRelation;
use Modules\Slider\Entities\Slider;

class Slider extends Model{

    use Translatable, NamespacedEntity, MediaRelation;

    protected $table = 'slider__sliders';
    public $translatedAttributes = [
        'title'
    ];
    protected $fillable = [
        'title'
    ];

    /**
     * Deleting files connections (from media__imageables table)
     * by deleting a main entity
     */
    public function delete() {
        foreach ($this->files as $file) {
            $file->pivot->delete();
        }
        return parent::delete();
    }

    public function slides() {
        return $this->hasMany('Modules\Slider\Entities\Slide')->orderBy('order', 'Asc');
    }
    
    /**
     * Function to selecting slides by slider_id (at homepage for example).
	 * Use it !!!
     */
    public static function getSlidesBySliderId($id){
        $slider = self::with(['slides.file'])->find($id);

        $filesIds = $slider->files->map(function ($file, $i) {
            return $file->pivot->file_id;
        })->toArray();

        $slides = Slide::with(['translations', 'file'])->whereIn('file_id', $filesIds)->get();
        
        return $slides;
    }
    

    protected static $entityNamespace = 'asgardcms/slider';

}
