<?php

namespace Modules\Slider\Entities;

use Illuminate\Database\Eloquent\Model;

class SliderTranslation extends Model {

    protected $table = 'slider__slider_translations';
    protected $fillable = [
        'title'
    ];

}