<?php

namespace Modules\Slider\Entities;

use Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;
//use Modules\Media\Entities\File;

class Slide extends Model {

    use Translatable;

    protected $table = 'slider__slides';
    public $translatedAttributes = ['title', 'content', 'url','body'];
    protected $fillable = [
        'slider_id',
        'file_id'
    ];

    public function file() {
        return $this->belongsTo('Modules\Media\Entities\File');
    }

//    public function files() {
//        return $this->morphToMany(File::class, 'imageable', 'media__imageables')->withPivot('zone', 'id')->withTimestamps()->orderBy('order');
//    }
//
//    public function getThumbnailAttribute() {
//        dd($this->files());
//        $thumbnail = $this->files()->where('zone', 'gallery')->first();
//
//        if ($thumbnail === null) {
//            $thumbnail = '/assets/img/default-thumbnail.jpg';
//            return $thumbnail;
//        }
//        return $thumbnail;
//    }

}
