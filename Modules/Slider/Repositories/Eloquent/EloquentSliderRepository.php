<?php

namespace Modules\Slider\Repositories\Eloquent;

use Illuminate\Database\Eloquent\Builder;
use Modules\Core\Repositories\Eloquent\EloquentBaseRepository;
use Modules\Slider\Events\SliderWasCreated;
use Modules\Slider\Events\SliderWasDeleted;
use Modules\Slider\Events\SliderWasUpdated;
use Modules\Slider\Repositories\SliderRepository;
use Modules\Slider\Entities\Slide;

class EloquentSliderRepository extends EloquentBaseRepository implements SliderRepository {

    /**
     * @param  mixed  $data
     * @return object
     */
    public function create($data) {
        $slider = $this->model->create($data);

        event(new SliderWasCreated($slider->id, $data, $slider));

//        $slider->setTags(array_get($data, 'tags', []));

        return $slider;
    }

    /**
     * @param $model
     * @param  array  $data
     * @return object
     */
    public function update($model, $data) {
        $model->update($data);

        event(new SliderWasUpdated($model->id, $data, $model));
        
        /**
         * Romoving slides for deleted files
         */
        Slide::where('slider_id', $model->id)->whereNotIn('file_id', $data['medias_multi']['gallery']['files'])->delete();
        
//        $model->setTags(array_get($data, 'tags', []));

        return $model;
    }

    public function destroy($slider) {
//        $slider->untag();

        event(new SliderWasDeleted($slider));

        return $slider->delete();
    }

    /**
     * @param $slug
     * @param $locale
     * @return object
     */
    public function findBySlugInLocale($slug, $locale) {
        if (method_exists($this->model, 'translations')) {
            return $this->model->whereHas('translations', function (Builder $q) use ($slug, $locale) {
                        $q->where('slug', $slug);
                        $q->where('locale', $locale);
                    })->with('translations')->first();
        }

        return $this->model->where('slug', $slug)->where('locale', $locale)->first();
    }

}
