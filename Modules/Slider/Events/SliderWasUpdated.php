<?php

namespace Modules\Slider\Events;

use Modules\Media\Contracts\StoringMedia;
use Modules\Slider\Entities\Slider;
use Modules\Slider\Entities\Slide;

class SliderWasUpdated implements StoringMedia {

    /**
     * @var array
     */
    public $data;

    /**
     * @var int
     */
    public $sliderId;

    /**
     * @var slider
     */
    public $slider;

    public function __construct($sliderId, array $data, $slider) {
        $this->data = $data;
//        foreach ($data['medias_multi']['gallery'] as $order => $file_id) {
//            $slide = Slide::firstOrCreate([
//              'slider_id' => $sliderId,
//              'file_id'   => $file_id,
//            ]);
//            $slide->order = $order;
//            $slide->save();
//        }
        $this->$sliderId = $sliderId;
        $this->slider = $slider;
    }

    /**
     * Return the ALL data sent
     * @return array
     */
    public function getSubmissionData() {
        return $this->data;
    }

    /**
     * Return the entity
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function getEntity() {
        return $this->slider;
    }

}
