<?php

namespace Modules\Slider\Events;

use Modules\Media\Contracts\DeletingMedia;

class SliderWasDeleted implements DeletingMedia {

    /**
     * @var string
     */
    private $sliderClass;

    /**
     * @var int
     */
    private $sliderId;

    /**
     * @var object
     */
    public $slider;

    public function __construct($slider) {
        $this->$slider = $slider;
        $this->sliderId = $slider->id;
        $this->postClass = get_class($slider);
    }

    /**
     * Get the entity ID
     * @return int
     */
    public function getEntityId() {
        return $this->sliderId;
    }

    /**
     * Get the class name the imageables
     * @return string
     */
    public function getClassName() {
        return $this->sliderClass;
    }

}