<?php

return [
    'title' => [
        'translations' => 'Tłumaczenia',
        'edit translation' => 'Edytuj tłumaczenie',
    ],
    'button' => [
        'create translation' => 'Utwórz tłumaczenie',
    ],
    'table' => [
    ],
    'form' => [
    ],
    'messages' => [
    ],
    'validation' => [
    ],
    'Clear translation cache' => 'Wyczyść dane tymczasowe tłumaczeń',
    'Export' => 'Export',
    'Import' => 'Import',
    'Import csv translations file' => 'Import csv translations file',
    'only for developers' => 'Will require git management!',
    'Translations exported' => 'Translations exported.',
    'Translations imported' => 'Translations imported.',
    'select CSV file' => 'Select CSV file ...',
    'csv only allowed' => 'Please upload a CSV file only.',
    'time' => 'Czas',
    'author' => 'Autor',
    'history' => 'historia',
    'No Revisions yet' => 'Jeszcze bez zmian',
    'event' => 'Zdarzenie',
    'created' => 'Utworzono',
    'edited' => 'Edytowano',
    'list resource' => 'List translations',
    'edit resource' => 'Edit translations',
    'import resource' => 'Import translations',
    'export resource' => 'Export translations',
];
