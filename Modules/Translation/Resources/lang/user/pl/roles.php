<?php

return [
    'button' => [
        'new-role' => 'Nowa rola',
    ],
    'title' => [
        'roles' => 'Role',
        'edit' => 'Edytuj role',
        'users-with-roles' => 'Urzytkownicy z tą rolą',
    ],
    'breadcrumb' => [
        'roles' => 'Role',
        'new' => 'Nowa',
        'edit' => 'Edycja roli',
    ],
    'table' => [
        'name' => 'Nazwa',
    ],
    'tabs' => [
        'data' => 'Dane',
        'permissions' => 'Dostępy',
    ],
    'form' => [
        'name' => 'Nazwa roli',
        'slug' => 'Nazwa systemowa',
    ],
    'navigation' => [
        'back to index' => 'Wróć do listy',
    ],
    'select all' => 'Zaznacz wszystko',
    'deselect all' => 'Odznacz wszystko',
    'swap' => 'Swap',
    'allow all' => 'Pozwól na wszystko',
    'deny all' => 'Zabroń wszystko',
    'inherit all' => 'Dziedzicz wszytko',
    'allow' => 'Pozwól',
    'deny' => 'Zabroń',
    'inherit' => 'Dziedzicz',
    'list resource' => 'List roles',
    'create resource' => 'Create roles',
    'edit resource' => 'Edit roles',
    'destroy resource' => 'Delete roles',
];
