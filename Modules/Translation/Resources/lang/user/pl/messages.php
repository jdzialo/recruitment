<?php

return [
    /* Authentication */
    'successfully logged in' => 'Zalogowano się poprawnie',
    'account created check email for activation' => 'Konto utworzone. Sprawdź pocztę email w celu aktywowania konta.',
    'account activated you can now login' => 'Konto aktywowane, Możesz się zalogować',
    'there was an error with the activation' => 'Urzytkownik już jest aktywowany',
    'no user found' => 'Nie znaleziono użytkownika',
    'check email to reset password' => 'Sprawdź pocztę email w celu zresetowania hasła.',
    'user no longer exists' => 'Użytkownik nie istnieje',
    'invalid reset code' => 'Błędny kod resetowania hasła.',
    'password reset' => 'Hasło zresetowane, możesz się zalogować na nowe hasło.',
    /* User management */
    'user created' => 'Użytkownik utworzony',
    'user not found' => 'Użytkownik',
    'user updated' => 'Użytkownik został zaktualizowany',
    'user deleted' => 'Użytkownik został usuniety',
    /* Role management */
    'role created' => 'Rola utworzona',
    'role not found' => 'Rola nie znaleziona',
    'role updated' => 'Rola została zaktualizowana.',
    'role deleted' => 'Rola została usunieta.',
    /* Profile management */
    'profile updated' => 'Profil został zaktualizowany',
];
