<?php

return [
    'site-name' => 'Nazwa strony',
    'site-description' => 'Opis strony',
    'site-name-mini' => 'Skrócona nazwa strony',
    'template' => 'Szablon strony',
    'analytics-script' => 'Kod Google Analytics',
    'locales' => 'Lokalizacje strony',
     'fb' => 'facebook',
    'twitter' => 'twitter'
];
