<?php

return [
    'main sidebar' => 'Newsletter',
    'list resource' => 'Lista emaili',
    'create resource' => 'Utwórz email',
    'edit resource' => 'Edytuj email',
    'destroy resource' => 'Usuń email',
    'write_your_email' => 'Podaj swój email',
    'title' => [
        'emails' => 'Email',
        'create email' => 'Utwórz email',
        'edit email' => 'Edytuj email',
    ],
    'button' => [
        'create email' => 'Utwórz email',
    ],
    'table' => [
      'sended at' => 'Data ostatniej wysyłki',
      'name' => 'Name',
    ],
    'form' => [
      'title' => 'Tytuł',
      'body' => 'Treść',
      'name' => 'Name'
    ],
    'messages' => [
    ],
    'validation' => [
    ],
];
