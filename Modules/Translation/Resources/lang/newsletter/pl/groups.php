<?php

return [
    'list resource' => 'Lisa grup',
    'create resource' => 'Utwórz grupę',
    'edit resource' => 'Edytuj grupę',
    'destroy resource' => 'Usuń grupę',
    'title' => [
        'groups' => 'Group',
        'create group' => 'Utwórz grupę',
        'edit group' => 'Edytuj grupę',
    ],
    'button' => [
        'create group' => 'Utwórz grupę',
    ],
    'table' => [
      'name' => 'Nazwa',
    ],
    'form' => [
      'name' => 'Nazwa'
    ],
    'messages' => [
    ],
    'validation' => [
    ],
];
