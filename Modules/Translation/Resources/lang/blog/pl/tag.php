<?php

return [
    'title' => [
        'tag' => 'Tagi wpisów',
        'create tag' => 'Utwórz nowy tag',
        'edit tag' => 'Edytuj tag',
    ],
    'button' => [
        'create tag' => 'Utwórz nowy tag',
    ],
    'table' => [
        'name' => 'Nazwa',
        'slug' => 'Nazwa systemowa',
    ],
    'form' => [
        'name' => 'Nazwa',
        'slug' => 'Nazwa systemowa',
    ],
    'messages' => [
    ],
    'validation' => [
    ],
    'list resource' => 'List tags',
    'create resource' => 'Create tags',
    'edit resource' => 'Edit tags',
    'destroy resource' => 'Delete tags',
];
