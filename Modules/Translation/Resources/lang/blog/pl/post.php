<?php

return [
    'title' => [
        'post' => 'Wpisy',
        'create post' => 'Utwórz wpis',
        'edit post' => 'Edytuj wpis',
    ],
    'button' => [
        'create post' => 'Utwórz wpis',
    ],
    'table' => [
        'title' => 'Tytuł',
        'slug' => 'Fragment Url',
        'status' => 'Status',
        'is_event' => 'Wydarzenie',
        'publish_at' => 'Data publikacji',
    ],
    'form' => [
        'title' => 'Tytuł',
        'slug' => 'Fragment Url',
        'meta_data' => 'Meta dane',
        'meta_title' => 'Meta tytuł',
        'meta_description' => 'Meta opis',
        'facebook_data' => 'Dane dla Facebook',
        'og_title' => 'Tytuł dla Facebook',
        'og_description' => 'Opis dla Facebook',
        'og_type' => 'Typ dla Facebook',
        'content' => 'Treść',
        'content_short' => 'Krótki opis',
        'is_event' => 'Jest wydarzeniem',
        'post_status' => 'Status wpisu',
    ],
    'facebook-types' => [
        'website' => 'Strona',
        'product' => 'Produkt',
        'article' => 'Artykuł',
    ],
    'navigation' => [
        'back to index' => 'Wróć do listy',
    ],
    'latest posts' => 'Ostatnie wpisy',
    'list resource' => 'Lista wpisów',
    'create resource' => 'Create posts',
    'edit resource' => 'Edit posts',
    'destroy resource' => 'Delete posts',
];
