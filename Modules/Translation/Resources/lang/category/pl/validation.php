<?php

return [
    'only one primary category' => 'Only one category can be primary at a time',
    'name is required' => 'Nazwa jest wymagana',
];
