<?php

return [
    'title'  => 'Kategoria',
    'titles' => [
        'category' => 'Manager kategorii',
        'create category' => 'Tworzenie kategorii',
        'edit category' => 'Edycja kategorii',
        'create category item' => 'Stwórz podkategorię',
        'edit category item' => 'Edycja podkategorii',
    ],
    'breadcrumb' => [
        'category' => 'Zarzadzanie categorią',
        'create category' => 'Utwórz kategorię',
        'edit category' => 'Edytuj kategorię',
        'create category item' => 'Utwórz podkategorię',
        'edit category item' => 'Edytuj podkategorię',
    ],
    'button' => [
        'create category item' => 'Utwórz podkategorię',
        'create category' => 'Utwórz kategorię',
    ],
    'table' => [
        'name' => 'Nazwa',
        'title' => 'Tytuł',
    ],
    'form' => [
        'title' => 'Tytuł',
        'name' => 'Nazwa',
        'status' => 'Aktywny',
        'uri' => 'URI',
        'url' => 'URL',
        'primary' => 'Primary category (used for front-end routing)',
    ],
    'navigation' => [
        'back to index' => 'Go back to the category index',
    ],
    'list resource' => 'List categorys',
    'create resource' => 'Create categorys',
    'edit resource' => 'Edit categorys',
    'destroy resource' => 'Delete categorys',
];
