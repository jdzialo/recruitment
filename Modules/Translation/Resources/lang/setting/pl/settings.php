<?php

return [
    'title' => [
        'settings' => 'Ustawienia',
        'general settings' => 'Główne ustawienia',
        'module settings' => 'Ustawinia modułów',
        'module name settings' => 'Ustawinia modułu :module',
    ],
    'breadcrumb' => [
        'settings' => 'Ustawinia',
        'module settings' => 'Ustwawinia modułu :module',
    ],
    'list resource' => 'List settings',
    'edit resource' => 'Edit settings',
];
