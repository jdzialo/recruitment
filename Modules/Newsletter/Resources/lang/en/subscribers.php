<?php

return [
    'list resource' => 'List subscribers',
    'create resource' => 'Create subscribers',
    'edit resource' => 'Edit subscribers',
    'destroy resource' => 'Destroy subscribers',
    'title' => [
        'subscribers' => 'Subscriber',
        'create subscriber' => 'Create a subscriber',
        'edit subscriber' => 'Edit a subscriber',
    ],
    'button' => [
        'create subscriber' => 'Create a subscriber',
    ],
    'table' => [
      'email' => 'Email',
      'name' => 'Name',
      'groups' => 'Groups',
    ],
    'form' => [
      'email' => 'Email',
      'first_name' => 'First name',
      'last_name' => 'Last name',
      'groups' => 'Groups'
    ],
    'messages' => [
    ],
    'validation' => [
    ],
];
