@extends('layouts.master')

@section('content-header')
    <h1>
        {{ trans('newsletter::subscribers.title.subscribers') }}
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{ route('dashboard.index') }}"><i class="fa fa-dashboard"></i> {{ trans('core::core.breadcrumb.home') }}</a></li>
        <li class="active">{{ trans('newsletter::subscribers.title.subscribers') }}</li>
    </ol>
@stop

@section('content')
    <div class="row">
        <div class="col-xs-12">
            <div class="row">
                <div class="btn-group pull-right" style="margin: 0 15px 15px 0;">
                    <a href="{{ route('admin.newsletter.subscriber.create') }}" class="btn btn-primary btn-flat" style="padding: 4px 10px;">
                        <i class="fa fa-pencil"></i> {{ trans('newsletter::subscribers.button.create subscriber') }}
                    </a>
                </div>
            </div>
            <div class="box box-primary">
                <div class="box-header">
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <table class="data-table table table-bordered table-hover">
                        <thead>
                        <tr>
                            <th>{{ trans('newsletter::subscribers.table.email') }}</th>
                            <th>{{ trans('newsletter::subscribers.table.name') }}</th>
                            <th>{{ trans('newsletter::subscribers.table.groups') }}</th>
                            <th>{{ trans('core::core.table.status') }}</th>
                            <th>{{ trans('core::core.table.created at') }}</th>
                            <th data-sortable="false">{{ trans('core::core.table.actions') }}</th>
                        </tr>
                        </thead>
                        <tbody>
                        @if(isset($subscribers))
                        @foreach ($subscribers as $subscriber)

                        <tr>
                          <td>
                              <a href="{{ route('admin.newsletter.subscriber.edit', [$subscriber->id]) }}">
                                  {{ $subscriber->email }}
                              </a>
                          </td>
                          <td>
                              <a href="{{ route('admin.newsletter.subscriber.edit', [$subscriber->id]) }}">
                                  {{ $subscriber->first_name.' '.$subscriber->last_name }}
                              </a>
                          </td>
                          <td>
                              @foreach ($subscriber->groups as $group)
                                <span class="label label-default">{{ $group->name }}</span>
                              @endforeach
                          </td>
                            <td>
                                <?php //dd("idnewsletera".$_statusClasses[$subscriber->status]); ?>
                                <a href="{{ route('admin.newsletter.subscriber.edit', [$subscriber->id]) }}">
                                    <span class="label label-{{ $_statusClasses[$subscriber->status] }}">{{ $_statuses[$subscriber->status] }}</span>
                                </a>
                            </td>
                            <td>
                                <a href="{{ route('admin.newsletter.subscriber.edit', [$subscriber->id]) }}">
                                    {{ $subscriber->created_at }}
                                </a>
                            </td>
                            <td>
                                <div class="btn-group">
                                    <a href="{{ route('admin.newsletter.subscriber.edit', [$subscriber->id]) }}" class="btn btn-default btn-flat"><i class="fa fa-pencil"></i></a>
                                    <button class="btn btn-danger btn-flat" data-toggle="modal" data-target="#modal-delete-confirmation" data-action-target="{{ route('admin.newsletter.subscriber.destroy', [$subscriber->id]) }}"><i class="fa fa-trash"></i></button>
                                </div>
                            </td>
                        </tr>
                        @endforeach
                        @endif
                        </tbody>
                        <tfoot>
                        <tr>
                            <th>{{ trans('newsletter::subscribers.table.email') }}</th>
                            <th>{{ trans('newsletter::subscribers.table.name') }}</th>
                            <th>{{ trans('newsletter::subscribers.table.groups') }}</th>
                            <th>{{ trans('core::core.table.status') }}</th>
                            <th>{{ trans('core::core.table.created at') }}</th>
                            <th data-sortable="false">{{ trans('core::core.table.actions') }}</th>
                        </tr>
                        </tfoot>
                    </table>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
        </div>
    </div>
    @include('core::partials.delete-modal')
@stop

@section('footer')
    <a data-toggle="modal" data-target="#keyboardShortcutsModal"><i class="fa fa-keyboard-o"></i></a> &nbsp;
@stop
@section('shortcuts')
    <dl class="dl-horizontal">
        <dt><code>c</code></dt>
        <dd>{{ trans('newsletter::subscribers.title.create subscriber') }}</dd>
    </dl>
@stop

@section('scripts')
    <script type="text/javascript">
        $( document ).ready(function() {
            $(document).keypressAction({
                actions: [
                    { key: 'c', route: "<?= route('admin.newsletter.subscriber.create') ?>" }
                ]
            });
        });
    </script>
    <?php $locale = locale(); ?>
    <script type="text/javascript">
        $(function () {
            $('.data-table').dataTable({
                "paginate": true,
                "lengthChange": true,
                "filter": true,
                "sort": true,
                "info": true,
                "autoWidth": true,
                "order": [[ 0, "desc" ]],
                "language": {
                    "url": '<?php echo Module::asset("core:js/vendor/datatables/{$locale}.json") ?>'
                }
            });
        });
    </script>
@stop
