<div class="box-body">
    <div class='form-group{{ $errors->has("email") ? ' has-error' : '' }}'>
        {!! Form::label("email", trans('newsletter::subscribers.form.email')) !!}
        {!! Form::email("email", old("email"), ['class' => 'form-control', 'data-slug' => 'source', 'placeholder' => trans('newsletter::subscribers.form.email')]) !!}
        {!! $errors->first("email", '<span class="help-block">:message</span>') !!}
    </div>

    <div class='form-group{{ $errors->has("first_name") ? ' has-error' : '' }}'>
        {!! Form::label("first_name", trans('newsletter::subscribers.form.first_name')) !!}
        {!! Form::text("first_name", old("first_name"), ['class' => 'form-control', 'data-slug' => 'source', 'placeholder' => trans('newsletter::subscribers.form.first_name')]) !!}
        {!! $errors->first("first_name", '<span class="help-block">:message</span>') !!}
    </div>

    <div class='form-group{{ $errors->has("last_name") ? ' has-error' : '' }}'>
        {!! Form::label("last_name", trans('newsletter::subscribers.form.last_name')) !!}
        {!! Form::text("last_name", old("last_name"), ['class' => 'form-control', 'data-slug' => 'source', 'placeholder' => trans('newsletter::subscribers.form.last_name')]) !!}
        {!! $errors->first("last_name", '<span class="help-block">:message</span>') !!}
    </div>

    <div class="form-group">
      <label for="groups">{{ trans('newsletter::subscribers.form.groups') }}</label>
      <select name="groups[]" id="groups" class="input-tags selectized" multiple="multiple">
        @foreach ($groups as $group)
          <option value="{{ $group->id }}">{{ $group->name }}</option>
        @endforeach
      </select>
    </div>
</div>
