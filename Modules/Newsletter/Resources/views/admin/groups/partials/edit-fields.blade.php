<div class="box-body">
    <div class="box-body">
          <div class='form-group{{ $errors->has("name") ? ' has-error' : '' }}'>
              {!! Form::label("name", trans('newsletter::groups.form.name')) !!}
              {!! Form::text("name", $group->name, ['class' => 'form-control', 'data-slug' => 'source', 'placeholder' => trans('newsletter::groups.form.name')]) !!}
              {!! $errors->first("name", '<span class="help-block">:message</span>') !!}
          </div>
    </div>
</div>
