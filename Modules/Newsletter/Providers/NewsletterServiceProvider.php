<?php

namespace Modules\Newsletter\Providers;

use Illuminate\Support\ServiceProvider;
use Modules\Core\Traits\CanPublishConfiguration;

class NewsletterServiceProvider extends ServiceProvider
{
    use CanPublishConfiguration;
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->registerBindings();
    }

    public function boot()
    {
        $this->publishConfig('newsletter', 'permissions');
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return array();
    }

    private function registerBindings()
    {
        $this->app->bind(
            'Modules\Newsletter\Repositories\EmailRepository',
            function () {
                $repository = new \Modules\Newsletter\Repositories\Eloquent\EloquentEmailRepository(new \Modules\Newsletter\Entities\Email());

                if (! config('app.cache')) {
                    return $repository;
                }

                return new \Modules\Newsletter\Repositories\Cache\CacheEmailDecorator($repository);
            }
        );
        $this->app->bind(
            'Modules\Newsletter\Repositories\SubscriberRepository',
            function () {
                $repository = new \Modules\Newsletter\Repositories\Eloquent\EloquentSubscriberRepository(new \Modules\Newsletter\Entities\Subscriber());

                if (! config('app.cache')) {
                    return $repository;
                }

                return new \Modules\Newsletter\Repositories\Cache\CacheSubscriberDecorator($repository);
            }
        );
        $this->app->bind(
            'Modules\Newsletter\Repositories\GroupRepository',
            function () {
                $repository = new \Modules\Newsletter\Repositories\Eloquent\EloquentGroupRepository(new \Modules\Newsletter\Entities\Group());

                if (! config('app.cache')) {
                    return $repository;
                }

                return new \Modules\Newsletter\Repositories\Cache\CacheGroupDecorator($repository);
            }
        );
// add bindings



    }
}
