<?php

use Illuminate\Routing\Router;

$router->group(['prefix' => '/newsletter'], function (Router $router) {
    $router->post('/subscribe', [
        'as' => 'newsletter.subscribe.create',
        'uses' => 'PublicController@create'
    ]);
    $router->post('/subscribe/store', [
        'as' => 'newsletter.subscribe.store',
        'uses' => 'PublicController@store'
    ]);
});