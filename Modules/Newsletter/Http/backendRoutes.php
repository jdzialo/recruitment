<?php

use Illuminate\Routing\Router;
/** @var Router $router */

$router->group(['prefix' =>'/newsletter'], function (Router $router) {
    $router->get('send/{id}', [
        'as' => 'admin.newsletter.send',
        'uses' => 'EmailController@send',
        'middleware' => 'can:newsletter.emails.destroy'
    ]);

    $router->bind('email', function ($id) {
        return app('Modules\Newsletter\Repositories\EmailRepository')->find($id);
    });
    $router->get('emails', [
        'as' => 'admin.newsletter.email.index',
        'uses' => 'EmailController@index',
        'middleware' => 'can:newsletter.emails.index'
    ]);
    $router->get('emails/create', [
        'as' => 'admin.newsletter.email.create',
        'uses' => 'EmailController@create',
        'middleware' => 'can:newsletter.emails.create'
    ]);
    $router->post('emails', [
        'as' => 'admin.newsletter.email.store',
        'uses' => 'EmailController@store',
        'middleware' => 'can:newsletter.emails.create'
    ]);
    $router->get('emails/{email}/edit', [
        'as' => 'admin.newsletter.email.edit',
        'uses' => 'EmailController@edit',
        'middleware' => 'can:newsletter.emails.edit'
    ]);
    $router->put('emails/{email}', [
        'as' => 'admin.newsletter.email.update',
        'uses' => 'EmailController@update',
        'middleware' => 'can:newsletter.emails.edit'
    ]);
    $router->delete('emails/{email}', [
        'as' => 'admin.newsletter.email.destroy',
        'uses' => 'EmailController@destroy',
        'middleware' => 'can:newsletter.emails.destroy'
    ]);
    $router->bind('subscriber', function ($id) {
        return app('Modules\Newsletter\Repositories\SubscriberRepository')->find($id);
    });
    $router->get('subscribers', [
        'as' => 'admin.newsletter.subscriber.index',
        'uses' => 'SubscriberController@index',
        'middleware' => 'can:newsletter.subscribers.index'
    ]);
    $router->get('subscribers/create', [
        'as' => 'admin.newsletter.subscriber.create',
        'uses' => 'SubscriberController@create',
        'middleware' => 'can:newsletter.subscribers.create'
    ]);
    $router->post('subscribers', [
        'as' => 'admin.newsletter.subscriber.store',
        'uses' => 'SubscriberController@store',
        'middleware' => 'can:newsletter.subscribers.create'
    ]);
    $router->get('subscribers/{subscriber}/edit', [
        'as' => 'admin.newsletter.subscriber.edit',
        'uses' => 'SubscriberController@edit',
        'middleware' => 'can:newsletter.subscribers.edit'
    ]);
    $router->put('subscribers/{subscriber}', [
        'as' => 'admin.newsletter.subscriber.update',
        'uses' => 'SubscriberController@update',
        'middleware' => 'can:newsletter.subscribers.edit'
    ]);
    $router->delete('subscribers/{subscriber}', [
        'as' => 'admin.newsletter.subscriber.destroy',
        'uses' => 'SubscriberController@destroy',
        'middleware' => 'can:newsletter.subscribers.destroy'
    ]);
    $router->bind('group', function ($id) {
        return app('Modules\Newsletter\Repositories\GroupRepository')->find($id);
    });
    $router->get('groups', [
        'as' => 'admin.newsletter.group.index',
        'uses' => 'GroupController@index',
        'middleware' => 'can:newsletter.groups.index'
    ]);
    $router->get('groups/create', [
        'as' => 'admin.newsletter.group.create',
        'uses' => 'GroupController@create',
        'middleware' => 'can:newsletter.groups.create'
    ]);
    $router->post('groups', [
        'as' => 'admin.newsletter.group.store',
        'uses' => 'GroupController@store',
        'middleware' => 'can:newsletter.groups.create'
    ]);
    $router->get('groups/{group}/edit', [
        'as' => 'admin.newsletter.group.edit',
        'uses' => 'GroupController@edit',
        'middleware' => 'can:newsletter.groups.edit'
    ]);
    $router->put('groups/{group}', [
        'as' => 'admin.newsletter.group.update',
        'uses' => 'GroupController@update',
        'middleware' => 'can:newsletter.groups.edit'
    ]);
    $router->delete('groups/{group}', [
        'as' => 'admin.newsletter.group.destroy',
        'uses' => 'GroupController@destroy',
        'middleware' => 'can:newsletter.groups.destroy'
    ]);
// append



});
