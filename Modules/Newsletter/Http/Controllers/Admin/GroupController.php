<?php

namespace Modules\Newsletter\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\Newsletter\Entities\Group;
use Modules\Newsletter\Repositories\GroupRepository;
use Modules\Core\Http\Controllers\Admin\AdminBaseController;

class GroupController extends AdminBaseController
{
    /**
     * @var GroupRepository
     */
    private $group;

    public function __construct(GroupRepository $group)
    {
        parent::__construct();

        $this->group = $group;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $groups = $this->group->all();

        return view('newsletter::admin.groups.index', compact('groups'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('newsletter::admin.groups.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $this->group->create($request->all());

        return redirect()->route('admin.newsletter.group.index')
            ->withSuccess(trans('core::core.messages.resource created', ['name' => trans('newsletter::groups.title.groups')]));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Group $group
     * @return Response
     */
    public function edit(Group $group)
    {
        return view('newsletter::admin.groups.edit', compact('group'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Group $group
     * @param  Request $request
     * @return Response
     */
    public function update(Group $group, Request $request)
    {
        $this->group->update($group, $request->all());

        return redirect()->route('admin.newsletter.group.index')
            ->withSuccess(trans('core::core.messages.resource updated', ['name' => trans('newsletter::groups.title.groups')]));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Group $group
     * @return Response
     */
    public function destroy(Group $group)
    {
        $this->group->destroy($group);

        return redirect()->route('admin.newsletter.group.index')
            ->withSuccess(trans('core::core.messages.resource deleted', ['name' => trans('newsletter::groups.title.groups')]));
    }
}
