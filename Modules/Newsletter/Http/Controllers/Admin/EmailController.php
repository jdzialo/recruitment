<?php

namespace Modules\Newsletter\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Mail;
use Modules\Newsletter\Entities\Email;
use Modules\Newsletter\Entities\Group;
use Modules\Newsletter\Mail\Newsletter;
use Modules\Newsletter\Entities\Subscriber;
use Modules\Newsletter\Repositories\EmailRepository;
use Modules\Core\Http\Controllers\Admin\AdminBaseController;

class EmailController extends AdminBaseController
{
    /**
     * @var EmailRepository
     */
    private $email;

    public function __construct(EmailRepository $email)
    {
        parent::__construct();

        $this->email = $email;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $emails = $this->email->all();
        $groups = Group::has('users')->get();

        return view('newsletter::admin.emails.index', compact('emails', 'groups'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('newsletter::admin.emails.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function store(Request $request)
    {
        $this->email->create($request->all());

        return redirect()->route('admin.newsletter.email.index')
            ->withSuccess(trans('core::core.messages.resource created', ['name' => trans('newsletter::emails.title.emails')]));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Email $email
     *
     * @return Response
     */
    public function edit(Email $email)
    {
        return view('newsletter::admin.emails.edit', compact('email'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Email   $email
     * @param Request $request
     *
     * @return Response
     */
    public function update(Email $email, Request $request)
    {
        $this->email->update($email, $request->all());

        return redirect()->route('admin.newsletter.email.index')
            ->withSuccess(trans('core::core.messages.resource updated', ['name' => trans('newsletter::emails.title.emails')]));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Email $email
     *
     * @return Response
     */
    public function destroy(Email $email)
    {
        $this->email->destroy($email);

        return redirect()->route('admin.newsletter.email.index')
            ->withSuccess(trans('core::core.messages.resource deleted', ['name' => trans('newsletter::emails.title.emails')]));
    }

    public function send($id)
    {
        $subscribers= Subscriber::whereHas('groups', function ($query) {
            $query->whereIn('group_id', request()->only('groups'));
        })->get();
        $mail = $this->email->find($id);
        foreach ($subscribers as $subscriber) {
           Mail::to($subscriber->email)->send(new Newsletter($mail));
        }
        $mail->update(['sended_at' => date('Y-m-d H:i:s')]);
        return response()->json(['status' => 'success', 'count' => $subscribers->count()]);
    }
}
