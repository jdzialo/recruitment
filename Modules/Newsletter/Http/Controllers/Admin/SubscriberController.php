<?php

namespace Modules\Newsletter\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\Newsletter\Entities\Subscriber;
use Modules\Newsletter\Entities\Group;
use Modules\Newsletter\Repositories\SubscriberRepository;
use Modules\Core\Http\Controllers\Admin\AdminBaseController;
use App\Status;

class SubscriberController extends AdminBaseController {

    /**
     * @var SubscriberRepository
     */
    private $subscriber;
    
    /**
     * @var Status
     */
    private $status;

    public function __construct(SubscriberRepository $subscriber, Status $status) {
        parent::__construct();

        $this->status = $status;
        $this->subscriber = $subscriber;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index() {
        $subscribers = $this->subscriber->all();
       
        $_statuses = $this->status->lists();
        
        $_statusClasses = $this->status->classes();
        
        return view('newsletter::admin.subscribers.index', compact('subscribers', '_statuses', '_statusClasses'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create() {
       
        $groups = Group::all();
        return view('newsletter::admin.subscribers.create', compact('groups'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request) {

        $subscriber = $this->subscriber->create($request->except('_token'));      
        $subscriber->groups()->sync($request->groups);
       
        return redirect()->route('admin.newsletter.subscriber.index')
                        ->withSuccess(trans('core::core.messages.resource created', ['name' => trans('newsletter::subscribers.title.subscribers')]));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Subscriber $subscriber
     * @return Response
     */
    public function edit(Subscriber $subscriber) {
        $groups = Group::all();
        $_statuses = $this->status->lists();
        
        return view('newsletter::admin.subscribers.edit', compact('subscriber', 'groups', '_statuses'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Subscriber $subscriber
     * @param  Request $request
     * @return Response
     */
    public function update(Subscriber $subscriber, Request $request) {
        $this->subscriber->update($subscriber, $request->all());
        $subscriber->groups()->sync($request->groups);

        return redirect()->route('admin.newsletter.subscriber.index')
                        ->withSuccess(trans('core::core.messages.resource updated', ['name' => trans('newsletter::subscribers.title.subscribers')]));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Subscriber $subscriber
     * @return Response
     */
    public function destroy(Subscriber $subscriber) {
        $this->subscriber->destroy($subscriber);

        return redirect()->route('admin.newsletter.subscriber.index')
                        ->withSuccess(trans('core::core.messages.resource deleted', ['name' => trans('newsletter::subscribers.title.subscribers')]));
    }

}
