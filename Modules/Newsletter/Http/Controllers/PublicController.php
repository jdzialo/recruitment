<?php

namespace Modules\Newsletter\Http\Controllers;

use Illuminate\Contracts\Foundation\Application;
use Modules\Core\Http\Controllers\BasePublicController;
use Modules\Newsletter\Entities\Subscriber;
use Modules\Newsletter\Repositories\SubscriberRepository;
use Illuminate\Http\Request;
use App\Status;

class PublicController extends BasePublicController {

    /**
     * @var SubscriberRepository
     */
    private $subscriber;

    /**
     * @var Application
     */
    private $app;

    public function __construct(Subscriber $subscriber, Application $app) {
        parent::__construct();
        
        $this->subscriber = $subscriber;
        $this->app = $app;
    }

    public function store(Request $request) {
       
        $subscriberAlreadyExist = Subscriber::where(['email' => $request->email])->get();

        if($subscriberAlreadyExist->isEmpty()){
            $subscriber = $this->subscriber->create(
                array_merge(
                    $request->all(), [
                        'status' => Status::ACTIVE // Set active status to new subscriber
                    ]
                )
            );
            return redirect()->route('homepage')
                            ->withSuccessn(trans('messages.newsletter.subscribed'));
        }else{
            return redirect()->route('homepage')
                            ->withInfon(trans('messages.newsletter.already_subscribed'));
        }
    }

}
