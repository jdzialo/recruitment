<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNewsletterEmailTranslationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('newsletter__email_translations', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('title');
            $table->text('body');

            $table->integer('email_id')->unsigned();
            $table->string('locale')->index();
            $table->unique(['email_id', 'locale']);
            $table->foreign('email_id')->references('id')->on('newsletter__emails')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('newsletter__email_translations', function (Blueprint $table) {
            $table->dropForeign(['email_id']);
        });
        Schema::dropIfExists('newsletter__email_translations');
    }
}
