<?php

namespace Modules\Newsletter\Entities;

use Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;

class Email extends Model
{
    use Translatable;

    protected $table = 'newsletter__emails';
    public $translatedAttributes = ['title', 'body'];
    protected $fillable = ['name', 'sended_at'];
}
