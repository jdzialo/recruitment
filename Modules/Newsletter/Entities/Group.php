<?php

namespace Modules\Newsletter\Entities;

use Illuminate\Database\Eloquent\Model;

class Group extends Model
{
    protected $table = 'newsletter__groups';
    protected $fillable = ['name'];

    public function users(){
        return $this->belongsToMany('Modules\Newsletter\Entities\Subscriber', 'newsletter__subscriber_group', 'group_id', 'subscriber_id');
    }
}
