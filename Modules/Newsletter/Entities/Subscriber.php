<?php

namespace Modules\Newsletter\Entities;

use Illuminate\Database\Eloquent\Model;

class Subscriber extends Model {

    protected $table = 'newsletter__subscribers';
    protected $fillable = [
        'email',
        'first_name',
        'last_name',
        'status'
    ];

    public function groups() {
        return $this->belongsToMany('Modules\Newsletter\Entities\Group', 'newsletter__subscriber_group');
    }

}
