<?php

return [
    'newsletter.emails' => [
        'index' => 'newsletter::emails.list resource',
        'create' => 'newsletter::emails.create resource',
        'edit' => 'newsletter::emails.edit resource',
        'destroy' => 'newsletter::emails.destroy resource',
    ],
    'newsletter.subscribers' => [
        'index' => 'newsletter::subscribers.list resource',
        'create' => 'newsletter::subscribers.create resource',
        'edit' => 'newsletter::subscribers.edit resource',
        'destroy' => 'newsletter::subscribers.destroy resource',
    ],
    'newsletter.groups' => [
        'index' => 'newsletter::groups.list resource',
        'create' => 'newsletter::groups.create resource',
        'edit' => 'newsletter::groups.edit resource',
        'destroy' => 'newsletter::groups.destroy resource',
    ],
// append



];
