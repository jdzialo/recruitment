<?php

namespace Modules\Newsletter\Repositories\Cache;

use Modules\Newsletter\Repositories\GroupRepository;
use Modules\Core\Repositories\Cache\BaseCacheDecorator;

class CacheGroupDecorator extends BaseCacheDecorator implements GroupRepository
{
    public function __construct(GroupRepository $group)
    {
        parent::__construct();
        $this->entityName = 'newsletter.groups';
        $this->repository = $group;
    }
}
