<?php

namespace Modules\Newsletter\Repositories\Eloquent;

use Modules\Newsletter\Repositories\EmailRepository;
use Modules\Core\Repositories\Eloquent\EloquentBaseRepository;

class EloquentEmailRepository extends EloquentBaseRepository implements EmailRepository
{
}
