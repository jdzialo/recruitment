<?php

namespace Modules\Newsletter\Repositories\Eloquent;

use Modules\Newsletter\Repositories\GroupRepository;
use Modules\Core\Repositories\Eloquent\EloquentBaseRepository;

class EloquentGroupRepository extends EloquentBaseRepository implements GroupRepository
{
}
