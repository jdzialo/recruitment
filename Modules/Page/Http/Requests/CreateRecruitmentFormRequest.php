<?php

namespace Modules\Page\Http\Requests;

use Modules\Core\Internationalisation\BaseFormRequest;

class CreateRecruitmentFormRequest extends BaseFormRequest {

     public function rules() {

       
        return [
          'email' => 'required|email'
        ];
    }

    public function authorize() {
        return true;
    }

    public function messages() {
        return [
           'email.required' => trans('page::contact.email is required'),
            'email.email' => trans('page::contact.email is not valid'),
        ];
    }

}
