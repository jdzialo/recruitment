<?php

namespace Modules\Page\Http\Requests;

use Modules\Core\Internationalisation\BaseFormRequest;

class CreateContactFormRequest extends BaseFormRequest {

    
    
    public function rules() {
        
        return [
            'name' => 'required|regex:/\s/',
            'email' => 'required|email',
            'subject' => 'required',
            'contactmessage' => 'required',
        ];
    }

    public function authorize() {
        return true;
    }

    public function messages() {
        return [
            'name.required' => trans('page::contact.name is required'),
            'name.regex' => trans('page::contact.name is not valid'),
            'email.required' => trans('page::contact.email is required'),
            'email.email' => trans('page::contact.email is not valid'),
            'contactmessage.required' => trans('page::contact.message is required'),
            'subject.required' => trans('page::contact.subject is required'),
            'subject.regex' => trans('page::contact.subject is not valid'),
        ];
    }

}
