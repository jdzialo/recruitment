<?php

use Illuminate\Routing\Router;

/** @var Router $router */
if (! App::runningInConsole()) {
    $router->get('/', [
        'uses' => 'PublicController@homepage',
        'as' => 'homepage',
        //'middleware' => config('asgard.page.config.middleware'),
        'middleware' => 'can:dashboard.index',
    ]);
     $router->get("recruitment", [
        'uses' => 'PublicController@recruitment',
        'as' => 'recruitment',
            //'middleware' => config('asgard.page.config.middleware'),
    ]);
     $router->get("about", [
        'uses' => 'PublicController@about',
        'as' => 'recruitment',
            //'middleware' => config('asgard.page.config.middleware'),
    ]);
    
     $router->get("contact", [
        'uses' => 'PublicController@contactpage',
        'as' => 'contact',
            //'middleware' => config('asgard.page.config.middleware'),
    ]);
    $router->post('contact/store', [
        'uses' => 'PublicController@storecontact',
        'as' => 'contactStore',
            //'middleware' => config('asgard.page.config.middleware'),
    ]);
     $router->post('recruitment/storerecruitment', [
        'uses' => 'PublicController@storeRecruitment',
        'as' => 'recruitmentStore',
            //'middleware' => config('asgard.page.config.middleware'),
    ]);
    
    $router->get('pages-lists', ['as'=>'pages-lists','uses'=>'PageSearchController@index']);

    $router->post('create-page', ['as'=>'create-page','uses'=>'PageSearchController@create']);
    
    
    $router->any('{uri}', [
        'uses' => 'PublicController@uri',
        'as' => 'page',
        'middleware' => config('asgard.page.config.middleware'),
    ])->where('uri', '.*');
}
