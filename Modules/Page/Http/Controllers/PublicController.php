<?php

namespace Modules\Page\Http\Controllers;

use Illuminate\Contracts\Foundation\Application;
use Modules\Core\Http\Controllers\BasePublicController;
use Modules\Menu\Repositories\MenuItemRepository;
use Modules\Blog\Repositories\PostRepository;
use Modules\Page\Entities\Page;
use Modules\Page\Repositories\PageRepository;
use Modules\Slider\Repositories\SliderRepository;
use Modules\Dashboard\Services\StatisticsService;
use Modules\Page\Http\Requests\CreateContactFormRequest;
use Modules\Page\Http\Requests\CreateRecruitmentFormRequest;
use Modules\Slider\Entities\Slider;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\App;



class PublicController extends BasePublicController {

    /**
     * @var PageRepository
     */
    private $page;

    /**
     * @var Application
     */
    private $app;

    /**
     * @var Application
     */
    public $email;

    /**
     * @var Application
     */
    public $companyEmail;

    /**
     * @var Application
     */
    public $file;
    private $cvFile;
    private $filePath;
    private $post;
    private $lang;
    public $slider;

    function getLang() {
        return $this->lang;
    }

    function setLang($lang) {
        $this->lang = $lang;
    }

    function getFilePath() {
        return $this->filePath;
    }

    function setFilePath($filePath) {
        $this->filePath = $filePath;
    }

    function getCvFile() {
        return $this->cvFile;
    }

    function setCvFile($cvFile) {
        $this->cvFile = $cvFile;
    }

    public function __construct(PageRepository $page, Application $app, PostRepository $post, SliderRepository $slider, \Modules\Setting\Contracts\Setting $setting) {
        parent::__construct($setting);
        $this->page = $page;
        $this->app = $app;
        $this->post = $post;
        $this->slider = $slider;
        $this->setLang(App::getLocale());
        
        
    }

    /**
     * @param $slug
     * @return \Illuminate\View\View
     */
    public function uri($slug) {
        
        
        $page = $this->findPageForSlug($slug);

        $this->throw404IfNotFound($page);

        $template = $this->getTemplateForPage($page);

        return view($template, compact('page'));
    }

    public function homepage() {
        $data = [];
        $page = $this->page->findHomepage();
        if (is_null($page)) {
            throw new \Exception('Brak załadowanej strony');
        }
        $data['page'] = $page;
        $this->throw404IfNotFound($page);
        $template = $this->getTemplateForPage($page);
        $slider = $this->slider();
        if (!is_null($slider)) {
            $data['slider'] = $slider;
        }
        $posts = $this->getPosts(3);
        if (!is_null($posts)) {

            $data['posts'] = $posts;
        }


        return view($template)->with($data);
    }

    /**
     * Find a page for the given slug.
     * The slug can be a 'composed' slug via the Menu
     * @param string $slug
     * @return Page
     */
    private function findPageForSlug($slug) {

        $menuItem = app(MenuItemRepository::class)->findByUriInLanguage($slug, locale());

        if ($menuItem) {
            return $this->page->find($menuItem->page_id);
        }

        return $this->page->findBySlug($slug);
    }

    /**
     * Return the template for the given page
     * or the default template if none found
     * @param $page
     * @return string
     */
    private function getTemplateForPage($page) {
        return (view()->exists($page->template)) ? $page->template : 'default';
    }

    /**
     * Throw a 404 error page if the given page is not found
     * @param $page
     */
    private function throw404IfNotFound($page) {
        if (is_null($page)) {
            $this->app->abort('404');
        }
    }

    public function about() {
        $lang = $this->getLocale();
        $page = $this->getPage();
        return view('about.index', compact('lang', 'page'));
    }

    public function recruitment() {
        $lang = $this->getLocale();
        $page = $this->getPage();
        return view('recruitement.index', compact('lang', 'page'));
    }

    public function contactpage() {

        $lang = $this->getLocale();
        $page = $this->getPage();

        return view('contact.index', compact('lang', 'page'));
    }

    public function storecontact(CreateContactFormRequest $request) {
        try {
            $reqestAll = $request->all();
            $this->email = $reqestAll['email'];
            \Mail::send('contact.email', $reqestAll, function($message) {
                $message->from($this->email);
                $message->to(env('MAIL_FROM_ADDRESS'), 'Admin')->subject(trans('page::contact.mail.subject'));
            });
        } catch (\Exception $e) {
            dd($e->getMessage());
            return redirect()->route('page', '/')
                            ->withError("Wystąpił błąd");
        }

        return \Redirect::route('page', '/')
                        ->with('message', trans('page::contact.contact.send contactform'));
    }

    public function storeRecruitment() {
        try {
            $this->setCvFile(Input::file('file'));
            $filePath = $this->getFile($this->saveFile($this->getCvFile()));
            $this->setFilePath($filePath);
            $reqestAll = [];
            $this->email = Input::get('email');
            $city = Input::get('city');
            $reqestAll['messageContent'] = Input::get('_message');
            $reqestAll['name'] = Input::get('_name');
            $reqestAll['email'] = $this->email;
            $companyEmail = $city . "@tradecorp.pl";
            $this->companyEmail = filter_var($companyEmail, FILTER_SANITIZE_EMAIL);
            \Mail::send('recruitement.email', $reqestAll, function($message) {
                $message->from($this->email);
                $message->to($this->companyEmail, 'Admin')->subject(trans('page::contact.mail.work_subject'));
                $message->attach($this->getFilePath());
            });
            $this->removeFile($this->getFilePath());
        } catch (\Exception $e) {
            // dd($e->getMessage());
            return redirect()->route('page', '/')
                            ->withError("Wystąpił błąd");
        }
        return \Redirect::route('page', '/')
                        ->with('message', trans('page::contact.contact.send recruitmentform'));
    }

    public function saveFile($file) {
        $destinationPath = 'assets/files';
        if (is_null($file)) {
            throw new \Exception('Brak załadowanego pliku');
        }
        $fileName = $file->getClientOriginalName();
        $fileName = urlencode($fileName);
        $filesize = $file->getSize();
        $file->move($destinationPath, $fileName);
        return $fileName;
    }

    public function getFile($fileName) {
        $pathFile = 'assets/files/' . $fileName;
        return $pathFile;
    }

    public function removeFile($filePath) {
        if (@file_exists($filePath)) {
            unlink($filePath);
            return true;
        }
        return false;
    }

    public function getLocale() {
        return App::getLocale();
    }

    public function getPage() {
        //$lang = App::getLocale();

        $page = $this->findPageForSlug($this->getURI());
        if (is_null($page)) {
            throw new \Exception('Brak załadowanej strony');
        }
        return $page;
    }

    public function getPosts($amount) {
        return $this->post->allTranslatedIn(App::getLocale())->paginate(3);
    }

    public function slider() {
        $slider = null;
        $slides = $slider = Slider::with(['slides', 'slides.translations', 'slides.file'])->orderBy('created_at', 'desc')->first()->get()[0]->slides()->get();
        return $slides;
    }

}
