<?php

namespace Modules\Core\Http\Controllers;

use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\App;
use Modules\User\Contracts\Authentication;
use Illuminate\Support\Facades\View;
use Modules\Setting\Contracts\Setting;

abstract class BasePublicController extends Controller
{
    /**
     * @var Authentication
     */
    protected $auth;
    public $locale;
    private $setting;

    public function __construct(\Modules\Setting\Contracts\Setting $setting = null)
    {
        $this->locale = App::getLocale();
        $this->auth = app(Authentication::class);
        $this->setting = $setting;
        $this->incudeFb();
    }
    
     public function getURI() {
       return ltrim($_SERVER['REQUEST_URI'], '/');
      
    }
    
    /**
     * @return \Illuminate\View\View
     */
    private function incudeFb()
    {       
        $fb = '';
        
          if(isset($this->setting) && $this->setting->has('core::fb'))
        {
            $fb = $this->setting->get('core::fb'); 
        }
        
          
          View::share('fb', $fb);
    }
   
}
