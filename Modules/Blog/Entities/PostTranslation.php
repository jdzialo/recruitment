<?php

namespace Modules\Blog\Entities;

use Illuminate\Database\Eloquent\Model;
use Laravel\Scout\Searchable;


class PostTranslation extends Model
{
    
    use Searchable;
    protected $primaryKey = 'id';
    public $timestamps = false;
    protected $fillable = ['title', 'slug', 'content', 'meta_title', 'meta_description', 'og_title', 'og_description', 'og_image', 'og_type'];
    protected $table = 'blog__post_translations';
}
