<?php

namespace Modules\Blog\Http\Controllers;

use Illuminate\Support\Facades\App;
use Modules\Blog\Repositories\PostRepository;
use Modules\Core\Http\Controllers\BasePublicController;
use Modules\Blog\Entities\Post;

class PublicController extends BasePublicController {

    /**
     * @var PostRepository
     */
    private $post;

    function getLocale() {
        return App::getLocale();
    }

    public function __construct(PostRepository $post) {
        parent::__construct();
        $this->post = $post;
    }

    public function index() {              
        $posts = $this->post->allTranslatedIn(App::getLocale())->paginate(9); 
        
        $locale = $this->getLocale();
        return view('blog.index', compact('posts','locale'));
    }

    public function show($slug) {

        $post = $this->post->findBySlug($slug);

        return view('blog.show', compact('post'));
    }

}
