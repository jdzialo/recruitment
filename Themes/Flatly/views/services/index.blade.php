@extends('layouts.master')
@section('content')
@include('partials.page-header',['page' => $page])
<div class="container">
    <div class="row row_default">
        <div class="col-md-7 col-lg-8 col_default">
            <div class="text">
                <div class="short">{!! $page->short !!}</div>
                {!! $page->body !!}
            </div>  
        </div>
        <div class="col-md-5 col-lg-4 col_napisz_do_nas">
            <div class="box_napisz_do_nas">
                <p>Masz pytania dotyczące naszej oferty?</p>
                <a href="{{route('contact')}}" class="btn_orange">Napisz do nas <i class="fa fa-angle-right" aria-hidden="true"></i></a>
            </div>
        </div>
    </div>
   <!-- <div class="row row_zadruki">
        <div class="col-xs-12 col-md-3 col_header">
            <h2>Zadruki na kartach</h2>
        </div>
        <div class="col-xs-12 col-md-9 col_text">
            <div class="text">
                <div class="img_wrapper">
                    <img src="/themes/flatly/img/uslugi.jpg" class="img-responsive">
                </div>
                <br/>
                <div class="text_light">
                    {!! $page->content !!}
<!--                    <ul class="clean">
                        <li>- zadruki na kartach magnetycznych, kartach dostępu, karta dla klientów,</li>
                        <li>- druk termosublimacyjny jedno- i dwustronny, kolorowy (CMYK), rozdzielczość 300dpi,</li>
                        <li>- druk termotransferowy jedno- i dwustronny, monochromatyczny, rozdzielczość 300dpi,</li>
                        <li>- projekty graficzne kart,</li>
                        <li>- programy lojalnościowe.</li>
                    </ul>

                    <p>W ramach organizacji programów lojalnościowych oferujemy pomoc w zakresie:</p>

                    <ul class="clean">
                        <li>- wydruku spersonalizowanych kart (awers - reklama, rewers - spersonalizowany kod kreskowy);</li>
                        <li>- przygotowania graficznego i wydruku materiałów marketingowych (foldery, plakaty, katalogi z prezentacją nagród);</li>
                        <li>- zakupu i magazynowania szerokiej gamy nagród wraz z ich wysyłką w ciągu 48h.</li>
                    </ul>
                </div>
            </div>
        </div>
    </div> -->
</div>
@endsection