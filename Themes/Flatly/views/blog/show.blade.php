@extends('layouts.master')

@section('title')
{{ $post->meta_title or $post->title }} | @parent
@endsection

@section('meta')
<meta name="title" content="{{ $post->meta_title}}" />
<meta name="description" content="{{ $post->meta_description }}" />
@endsection

@section('content')
<div class="container-fluid container_page_header" style="background-image: url('/themes/flatly/img/bg/o_firmie.jpg')">
    <div class="container">
        <div class="row row_page_header">
            <div class="col-xs-12 col_page_header">
                <h1 class="page_h1">
                    <div class="title">
                        Aktualności
                    </div>
                </h1>
            </div>
        </div>
    </div>
    <div class="mask_yellow_triangle"></div>
</div>
{{-- dd($post) --}}
<div class="container">
    <div class="row row_header">
        <div class="col-xs-12 col_l">
            <a href="{{ URL::route($currentLocale . '.blog') }}" class="btn_orange">
                <i class="fa fa-angle-left" aria-hidden="true"></i> Wróć do listy
            </a>
        </div>
    </div>

    <div class="row row_default">
        <div class="col-xs-12 col-md-7 col-lg-8 col_default">
            <div class="text">
                <div class="header">
                    <div class="title">{{ $post->title }}</div>
                    <span class="date">{{ $post->created_at->format('d-m-Y') }}</span>
                </div>
                {!! $post->content !!}
            </div>
        </div>
        <div class="hidden-xs hidden-sm col-md-5 col-lg-4 col_r">
            <a href="{{route('contact')}}" class="box_dolacz_do_nas">
                <p>Chcesz się rozwijać, szukasz kreatywnego zajęcia?</p>
                <p>DOŁĄCZ DO NAS!</p>
            </a>
        </div>
    </div>


    <div class="row row_pagination">
        <div class="col-xs-6 col_l">
            <?php if ($previous = $post->present()->previous): ?>
                <a href="{{ route(locale() . '.blog.slug', [$previous->slug]) }}" class="btn_orange">
                    <i class="fa fa-angle-left" aria-hidden="true"></i> Poprzedni
                </a>
            <?php endif; ?>
        </div>

        <div class="col-xs-6 col_r">
            <?php if ($next = $post->present()->next): ?>
                <a href="{{ route(locale() . '.blog.slug', [$next->slug]) }}" class="btn_orange pull-right">
                    Następny <i class="fa fa-angle-right" aria-hidden="true"></i>
                </a>
            <?php endif; ?>
        </div>
    </div>
</div>
@endsection


@section('scripts')
@push('scripts')
<script type="text/javascript">
    $(document).ready(function () {
        $('#backSearch').click(function () {
            window.history.back();
        });
    });
</script>
@endpush
@endsection