@extends('layouts.master')

@section('title')
Blog posts | @parent
@endsection

@section('content')
<div class="container-fluid container_page_header" style="background-image: url('/themes/flatly/img/bg/o_firmie.jpg')">
    <div class="container">
        <div class="row row_page_header">
            <div class="col-xs-12 col_page_header">
                <h1 class="page_h1">
                    <div class="title">
                        Aktualności
                    </div>
                </h1>
            </div>
        </div>
    </div>
    <div class="mask_yellow_triangle"></div>
</div>


<div class="container">
    <div class="row row_default">
        @if($posts->count())
        @foreach ($posts as $post)
        <div class="col-xs-12 col-sm-6 col-lg-4 col_news">
            <div class="box_home_news">
                @if ($post->getThumbnailAttribute() != null)
                <img src="{{ $post->getThumbnailAttribute()->path }}" class="img-responsive">
                @endif
                <div class="mask_pattern"></div>
                <div class="wrapper_content">
                    <div class="box_content">
                        <div class="date">{{ $post->created_at->format('d-m-Y') }}</div>
                        <div class="title"><a href="{{ URL::route($locale . '.blog.slug', [$post->slug]) }}">{{ $post->title }}</a> </div>
                        <div class="short">{!! substr($post->content, 0, 100) !!} </div>
                    </div>
                </div>
            </div>
        </div>
        @endforeach
        @endif
    </div>
    <div class="row box_pagination">
        <div class="col-xs-12">
            {!! with(new App\Pagination\HDPresenter($posts))->render(); !!} 
        </div>
    </div>
</div>
@endsection
