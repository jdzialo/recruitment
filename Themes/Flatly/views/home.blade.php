@extends('layouts.master')


@section('title')
{{ $page->title }} | @parent
@endsection
@section('meta')
<meta name="title" content="{{ $page->meta_title}}" />
<meta name="description" content="{{ $page->meta_description }}" />
@endsection

@section('content')
@include('partials.slider')
<div class="container">
    <div class="row row_home_news_header">
        <div class="col-xs-12 col_title">
            <h2>Co nowego w TradeCorp? <a href="{{ URL::route(locale() . '.blog') }}" class="hidden-xs btn_read_more pull-right">Czytaj więcej <i class="fa fa-angle-right" aria-hidden="true"></i></a></h2>
        </div>
    </div>
    <div class="row row_home_news_item">      
        @include('partials.blog', ['items' => $posts, 'locale' => locale()])
    </div>
    <div class="row row_read_more visible-xs">
        <div class="col-xs-12 col_read_more">
            <a href="{{ URL::route(locale() . '.blog') }}" class="btn_read_more pull-right">Czytaj więcej <i class="fa fa-angle-right" aria-hidden="true"></i></a>
        </div>
    </div>
</div>
<div class="box_gmap">
    <div id="gmap_home"></div>
    <a class="btn_gmap_link" href="{{route('contact')}}">Umów się z nami na spotkanie <i class="fa fa-angle-right" aria-hidden="true"></i></a>
</div>
@endsection

@push('scripts')
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAxrjy4S3oZppA94ztine1IIr2oAdx4NPc&region=PL"></script>
{!! Theme::script('vendor/gmap3/dist/gmap3.min.js') !!}

<script>
$(document).ready(function () {
    $('#gmap_home').gmap3({
        map: {
            address: "Rzeszów",
            options: {
                zoom: 12,
                scrollwheel: false,
                mapTypeId: "style1",
                mapTypeControlOptions: {
                    mapTypeIds: [google.maps.MapTypeId.ROADMAP]
                }
            }
        },
        marker: {address: "Rzeszów", options: {icon: "/themes/flatly/img/icons/marker.png"}},
        styledmaptype: {
            id: "style1",
            options: {name: "Map"},
            styles: gmapStyle
        }
    });
});
</script>
<script>
    var gmapStyle = [{"featureType":"water","elementType":"all","stylers":[{"hue":"#7fc8ed"},{"saturation":55},{"lightness":-6},{"visibility":"on"}]},{"featureType":"water","elementType":"labels","stylers":[{"hue":"#7fc8ed"},{"saturation":55},{"lightness":-6},{"visibility":"off"}]},{"featureType":"poi.park","elementType":"geometry","stylers":[{"hue":"#83cead"},{"saturation":1},{"lightness":-15},{"visibility":"on"}]},{"featureType":"landscape","elementType":"geometry","stylers":[{"hue":"#f3f4f4"},{"saturation":-84},{"lightness":59},{"visibility":"on"}]},{"featureType":"landscape","elementType":"labels","stylers":[{"hue":"#ffffff"},{"saturation":-100},{"lightness":100},{"visibility":"off"}]},{"featureType":"road","elementType":"geometry","stylers":[{"hue":"#ffffff"},{"saturation":-100},{"lightness":100},{"visibility":"on"}]},{"featureType":"road","elementType":"labels","stylers":[{"hue":"#bbbbbb"},{"saturation":-100},{"lightness":26},{"visibility":"on"}]},{"featureType":"road.arterial","elementType":"geometry","stylers":[{"hue":"#ffcc00"},{"saturation":100},{"lightness":-35},{"visibility":"simplified"}]},{"featureType":"road.highway","elementType":"geometry","stylers":[{"hue":"#ffcc00"},{"saturation":100},{"lightness":-22},{"visibility":"on"}]},{"featureType":"poi.school","elementType":"all","stylers":[{"hue":"#d7e4e4"},{"saturation":-60},{"lightness":23},{"visibility":"on"}]}];
</script>
@endpush

