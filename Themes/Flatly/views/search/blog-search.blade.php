@extends('layouts.master')
@section('content')
<div class="container-fluid container_page_header" style="background-image: url('/themes/flatly/img/bg/o_firmie.jpg')">
    <div class="container">
        <div class="row row_page_header">
            <div class="col-xs-12 col_page_header">
                <h1 class="page_h1">
                    <div class="title">
                        Wyniki wyszukiwania
                    </div>
                </h1>
            </div>
        </div>
    </div>
    <div class="mask_yellow_triangle"></div>
</div>


<div class="container">
    <div class="row row_default">
        
    @if($items->count())
    @foreach($items as $key => $item)
    <div class="col-md-4">
        <div class="box_home_news">
            @if($item->path != null) <img src="<?php echo $item->path; ?>" class="img-responsive"> @endif
            <div class="mask_pattern"></div>
            <div class="wrapper_content">
                <div class="box_content">
                    <div class="date">{{ $item->created_at->format('d-m-Y') }}</div>
                    <div class="title"><a href="{{ URL::route($locale . '.blog.slug', [$item->slug]) }}">{{ $item->title }}</a> </div>
                    <div class="short"><?php echo substr($item->content, 0, 100); ?>  </div>
                </div>
            </div>
        </div>
    </div>
    @endforeach
     <div class="row box_pagination">
            <div class="col-xs-12">
            
                {!! with(new App\Pagination\HDPresenter($items))->render(); !!} 
            </div>
        </div>
    @else
    <div class="col-xs-12">
        <p class="bg-info">Nie znaleziono szukanej frazy.</p>
    </div>
    @endif               
    
   
    </div>
</div>   
@endsection