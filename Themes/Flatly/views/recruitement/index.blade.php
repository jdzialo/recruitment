@extends('layouts.master')
@section('content')
@include('partials.page-header',['page' => $page])
<div class="container">
    <div class="row row_default">
        <div class="col-xs-12 col-md-7 col-lg-8 col_default">
            <div class="text">
                <div class="short">{!! $page->short !!}</div>
                {!! $page->body !!}
            </div>
        </div>
        <div class="hidden-xs hidden-sm col-md-5 col-lg-4 col_r">
            <a href="{{route('contact')}}" class="box_dolacz_do_nas">
                <p>Chcesz się rozwijać, szukasz kreatywnego zajęcia?</p>
                <p>DOŁĄCZ DO NAS!</p>
            </a>
        </div>
    </div>
    <div class="row row_acordion">
        <div class="col-xs-12">
            <h2>Prześlij nam Swoje CV</h2>
        </div>
        <div class="col-xs-12">
            <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="hrzeszow" data-city="Rzeszów" data-toggle="collapse" data-parent="#accordion" href="#crzeszow" aria-expanded="true" aria-controls="crzeszow">
                        <h4 class="panel-title">
                            <i class="fa fa-caret-down" aria-hidden="true"></i>
                            <a class="title_link" data-city="Rzeszów">
                                Rzeszów
                            </a>
                        </h4>
                    </div>
                    <div id="crzeszow" class="panel-collapse collapse" role="tabpanel" aria-labelledby="hrzeszow">
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-7">
                                    {!! Form::open(['id' => 'rzeszow', 'route' => 'recruitmentStore', 'method' => 'post', 'class' => 'contactform','files' => true ]) !!}
                                    @include('recruitement.partials.create-fields', ['lang' => locale(), 'errors' => $errors , 'city'=> 'rzeszow'])
                                    <div class="form-group">
                                        <input type="submit" value="{{ trans('page::contact.button.send2') }}">
                                    </div>
                                    {!! Form::close() !!}
                                </div>
                                <div class="col-md-5">
                                    <div class="box_gmap_recruiment">
                                        <div class="gmap_recruiment" id="gmap_rzeszow"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="hjaslo" data-city="Jasło" data-toggle="collapse" data-parent="#accordion" href="#cjaslo" aria-expanded="true" aria-controls="cjaslo">
                        <h4 class="panel-title">
                            <i class="fa fa-caret-down" aria-hidden="true"></i>
                            <a class="title_link" data-city="Jasło">
                                Jasło
                            </a>
                        </h4>
                    </div>
                    <div id="cjaslo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="hjaslo">
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-7">
                                    {!! Form::open(['id' => 'jaslo', 'route' => 'recruitmentStore', 'method' => 'post', 'class' => 'contactform','files' => true ]) !!}
                                    @include('recruitement.partials.create-fields', ['lang' => locale(), 'errors' => $errors , 'city'=> 'jaslo'])
                                    <div class="form-group">
                                        <input type="submit" value="{{ trans('page::contact.button.send2') }}">
                                    </div>
                                    {!! Form::close() !!}
                                </div>
                                <div class="col-md-5">
                                    <div class="box_gmap_recruiment">
                                        <div class="gmap_recruiment" id="gmap_jaslo"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="hkrosno" data-city="Krosno" data-toggle="collapse" data-parent="#accordion" href="#ckrosno" aria-expanded="false" aria-controls="ckrosno">
                        <h4 class="panel-title">
                            <i class="fa fa-caret-down" aria-hidden="true"></i>
                            <a class="collapsed title_link" data-city="Krosno">
                                Krosno
                            </a>
                        </h4>
                    </div>
                    <div id="ckrosno" class="panel-collapse collapse" role="tabpanel" aria-labelledby="hkrosno">
                        <div class="panel-body">   
                            <div class="row">
                                <div class="col-md-7">
                                    {!! Form::open(['id' => 'krosno' , 'route' => 'recruitmentStore', 'method' => 'post', 'class' => 'contactform' , 'files' => true ]) !!}
                                    @include('recruitement.partials.create-fields', ['lang' => locale(), 'errors' => $errors , 'city'=> 'krosno' ])
                                    <div class="form-group">
                                        <input id="submit2" type="submit" value="{{ trans('page::contact.button.send2') }}">
                                    </div>
                                    {!! Form::close() !!}
                                </div>
                                <div class="col-md-5">
                                    <div class="box_gmap_recruiment">
                                        <div class="gmap_recruiment" id="gmap_krosno"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="hprzemysl" data-city="Przemyśl" data-toggle="collapse" data-parent="#accordion" href="#cprzemysl" aria-expanded="false" aria-controls="cprzemysl">
                        <h4 class="panel-title">
                            <i class="fa fa-caret-down" aria-hidden="true"></i>
                            <a class="collapsed title_link" data-city="Przemyśl">
                                Przemyśl
                            </a>
                        </h4>
                    </div>
                    <div id="cprzemysl" class="panel-collapse collapse" role="tabpanel" aria-labelledby="hprzemysl">
                        <div class="panel-body">   
                            <div class="row">
                                <div class="col-md-7">
                                    {!! Form::open(['id' => 'przemysl' , 'route' => 'recruitmentStore', 'method' => 'post', 'class' => 'contactform' , 'files' => true ]) !!}
                                    @include('recruitement.partials.create-fields', ['lang' => locale(), 'errors' => $errors , 'city'=> 'przemysl' ])
                                    <div class="form-group">
                                        <input id="submit2" type="submit" value="{{ trans('page::contact.button.send2') }}">
                                    </div>
                                    {!! Form::close() !!}
                                </div>
                                <div class="col-md-5">
                                    <div class="box_gmap_recruiment">
                                        <div class="gmap_recruiment" id="gmap_przemysl"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="hsanok" data-city="Sanok" data-toggle="collapse" data-parent="#accordion" href="#csanok" aria-expanded="false" aria-controls="csanok">
                        <h4 class="panel-title">
                            <i class="fa fa-caret-down" aria-hidden="true"></i>
                            <a class="collapsed title_link" data-city="Sanok">
                                Lesko i Sanok
                            </a>
                        </h4>
                    </div>
                    <div id="csanok" class="panel-collapse collapse" role="tabpanel" aria-labelledby="hsanok">
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-7">
                                    {!! Form::open(['id' => 'sanok',  'route' => 'recruitmentStore', 'method' => 'post', 'class' => 'contactform' , 'files' => true ]) !!}
                                    @include('recruitement.partials.create-fields', ['lang' => locale(), 'errors' => $errors , 'city'=> 'lesko-sanok' ])
                                    <div class="form-group">    
                                        <input id="submit1" type="submit" value="{{ trans('page::contact.button.send2') }}">
                                    </div>
                                    {!! Form::close() !!}
                                </div>
                                <div class="col-md-5">
                                    <div class="box_gmap_recruiment">
                                        <div class="gmap_recruiment" id="gmap_sanok"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
@push('scripts')
{!! Theme::script('vendor/jquery-validation/dist/jquery.validate.min.js') !!}
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAxrjy4S3oZppA94ztine1IIr2oAdx4NPc&region=PL"></script>
{!! Theme::script('vendor/gmap3/dist/gmap3.min.js') !!}
<script>
    $(document).ready(function () {
        var data = {
            rules: {
                email: {
                    required: true,
                    email: true
                },
                _name: "required",
                _message: "required",
                file: {
                    required: true,
                    //  extension: "pdf"
                }
            },
            submitHandler: function (form) {
                form.submit();
            }
        };

        $("#rzeszow").validate(data);
        $("#sanok").validate(data);
        $("#przemysl").validate(data);
        $("#krosno").validate(data);
        $("#jaslo").validate(data);


        $("#submit3").click(function () {
            $('#rzeszow').submit();
        });

        $("#submit2").click(function () {
            $('#krosno').submit();
        });
        $("#submit1").click(function () {
            $('#sanok').submit();
        });
        
        
        /*$('#gmap_rzeszow').gmap3({
            map: {
                address: "Rzeszów",
                options: {
                    zoom: 12,
                    scrollwheel: false,
                    mapTypeId: "style1",
                    mapTypeControlOptions: {
                        mapTypeIds: ["style1"]
                    }
                }
            },
            marker: {address: "Rzeszów", options: {icon: "/themes/flatly/img/icons/marker.png"}},
            styledmaptype: {
                id: "style1",
                options: {name: "Map"},
                styles: gmapStyle
            }
        });  */      
        
        $('.panel-heading').click(function () {
            $(this).parent().find('.gmap_recruiment').gmap3({
                map: {
                    address: $(this).attr('data-city'),
                    options: {
                        zoom: 12,
                        scrollwheel: false,
                        mapTypeId: "style1",
                        mapTypeControlOptions: {mapTypeIds: ["style1"]}
                    }
                },
                marker: {address: $(this).attr('data-city'), options: {icon: "/themes/flatly/img/icons/marker.png"}},
                styledmaptype: {
                    id: "style1",
                    options: {name: "Map"},
                    styles: gmapStyle
                }
            });
        });
        
        
        $(".panel-collapse").on('show.bs.collapse', function() {
            $(this).siblings().find(".panel-title i.fa").css('transform', 'rotate(180deg)');
        }).on('hide.bs.collapse', function() {
            $(this).siblings().find(".panel-title i.fa").css('transform', 'rotate(0deg)');
        });
    });
</script>
<script>
    var gmapStyle = [{"featureType":"water","elementType":"all","stylers":[{"hue":"#7fc8ed"},{"saturation":55},{"lightness":-6},{"visibility":"on"}]},{"featureType":"water","elementType":"labels","stylers":[{"hue":"#7fc8ed"},{"saturation":55},{"lightness":-6},{"visibility":"off"}]},{"featureType":"poi.park","elementType":"geometry","stylers":[{"hue":"#83cead"},{"saturation":1},{"lightness":-15},{"visibility":"on"}]},{"featureType":"landscape","elementType":"geometry","stylers":[{"hue":"#f3f4f4"},{"saturation":-84},{"lightness":59},{"visibility":"on"}]},{"featureType":"landscape","elementType":"labels","stylers":[{"hue":"#ffffff"},{"saturation":-100},{"lightness":100},{"visibility":"off"}]},{"featureType":"road","elementType":"geometry","stylers":[{"hue":"#ffffff"},{"saturation":-100},{"lightness":100},{"visibility":"on"}]},{"featureType":"road","elementType":"labels","stylers":[{"hue":"#bbbbbb"},{"saturation":-100},{"lightness":26},{"visibility":"on"}]},{"featureType":"road.arterial","elementType":"geometry","stylers":[{"hue":"#ffcc00"},{"saturation":100},{"lightness":-35},{"visibility":"simplified"}]},{"featureType":"road.highway","elementType":"geometry","stylers":[{"hue":"#ffcc00"},{"saturation":100},{"lightness":-22},{"visibility":"on"}]},{"featureType":"poi.school","elementType":"all","stylers":[{"hue":"#d7e4e4"},{"saturation":-60},{"lightness":23},{"visibility":"on"}]}];
</script>
@endpush

@push('scripts')
@if ($lang == "en")
{!! Theme::script('vendor/jquery-validation/src/localization/messages_en.js') !!}
@endif
@if ($lang == "pl")
{!! Theme::script('vendor/jquery-validation/src/localization/messages_pl.js') !!}
@endif
@endpush

