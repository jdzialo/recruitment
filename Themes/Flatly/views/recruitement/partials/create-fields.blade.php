<div class="box-body">
    {{ Form::hidden('city', $city) }}
  
    <div class='form-group{{ $errors->has("_name") ? ' has-error' : '' }}'>
        <div class="row">
            <div class="col-md-4 col-lg-3">
                {!! Form::label('_name', trans('page::contact.form._name'), array('class' => '')) !!}
            </div>
            <div class="col-md-8 col-md-9">
                {!! Form::text("_name", old("_name"), ['class' => ' form-control circle-input', 'onfocus' => 'this.placeholder=""', 'onblur' => 'this.placeholder=""', 'placeholder' => trans('page::contact.form.name')]) !!}
            </div>
        </div>
        {!! $errors->first("_name", '<span class="help-block">:message</span>') !!}
    </div>

    <div class='form-group{{ $errors->has("email") ? ' has-error' : '' }}'>
        <div class="row">
            <div class="col-md-4 col-lg-3">
                {!! Form::label("email", trans('page::contact.form.email'), array('class' => '')) !!}
            </div>
            <div class="col-md-8 col-md-9">
                {!! Form::email("email", old("email"), ['class' => 'form-control circle-input', 'onfocus' => 'this.placeholder=""', 'onblur' => 'this.placeholder=""',  'placeholder' => trans('page::contact.form.email')]) !!}
            </div>
        </div>
        {!! $errors->first("email", '<span class="help-block">:message</span>') !!}
    </div>

    <div class='form-group{{ $errors->has("file") ? ' has-error' : '' }}'>
        <div class="row">
            <div class="col-md-4 col-lg-3">
                {!! Form::label("file", trans('page::contact.form.file_cv'), array('class' => '')) !!}
            </div>
            <div class="col-md-8 col-md-9">
                {!! Form::file('file') !!}
            </div>
        </div>
        {!! $errors->first("file", '<span class="help-block">:message</span>') !!}
    </div>

    <div class='form-group{{ $errors->has("_message") ? ' has-error' : '' }}'>
        <div class="row">
            <div class="col-md-4 col-lg-3">
                {!! Form::label('_message', trans('page::contact.form.message'), array('class' => '')) !!}
            </div>
            <div class="col-md-8 col-md-9">
                {!! Form::textarea("_message", old("_message"), ['class' => 'form-control circle-input-textarea', 'onfocus' => 'this.placeholder=""', 'onblur' => 'this.placeholder=""',  'placeholder' => trans('page::contact.form.message'), 'rows' => '5']) !!}
            </div>
        </div>
        {!! $errors->first("_message", '<span class="help-block">:message</span>') !!}
    </div>

</div>

<style>
    .requiredfield:before { content:"*";  color:red}
</style>​


