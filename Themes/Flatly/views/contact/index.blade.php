@extends('layouts.master')
@section('content')
@include('partials.page-header',['page' => $page])
<div class="container">
    <div class="row row_default">
        <div class="col-xs-12 col-sm-6 contactdiv">
            <div class="text">
                <div class="short">{!! $page->short !!}</div>
                <!--<b>„TradeCorp” Sp. z o.o.</b><br/>-->
                <!--ul. Osmeckiego 6B,<br/>-->
                <!--35-205 Rzeszów<br/>-->
                <!--Tel. 17 123 123 11<br/>-->
                <!--Mail: <a href="mailto:kontakt@tradecorp.pl">kontakt@tradecorp.pl</a>-->
                {!! $page->body !!}
            </div>
        </div>
        <div class="col-xs-12 col-sm-6 col_form">
            {!! Form::open(['route' => 'contactStore', 'method' => 'post', 'class' => 'contactform']) !!}
            @include('contact.partials.create-fields', ['lang' => locale(), 'errors' => $errors])
            {!! Form::close() !!}
        </div>
    </div>
</div>

<div class="box_gmap">
    <div id="gmap_contact"></div>
    <div class="gmap_buttons">
        <button class="btn_gmap" data-city="Rzeszów"><img src="/themes/flatly/img/icons/marker_mini.png"> Rzeszów</button>
        <button class="btn_gmap" data-city="Sanok"><img src="/themes/flatly/img/icons/marker_mini.png"> Sanok</button>
        <button class="btn_gmap" data-city="Krosno"><img src="/themes/flatly/img/icons/marker_mini.png"> Krosno</button>
        <button class="btn_gmap" data-city="Przemyśl"><img src="/themes/flatly/img/icons/marker_mini.png"> Przemyśl</button>
        <button class="btn_gmap" data-city="Jasło"><img src="/themes/flatly/img/icons/marker_mini.png"> Jasło</button>
    </div>
</div>

@endsection
@push('scripts')
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAxrjy4S3oZppA94ztine1IIr2oAdx4NPc&region=PL"></script>
{!! Theme::script('vendor/gmap3/dist/gmap3.min.js') !!}
<script>
    $(document).ready(function () {
        $('#gmap_contact').gmap3({
            map: {
                address: "Rzeszów",
                options: {
                    zoom: 12,
                    scrollwheel: false,
                    mapTypeId: "style1",
                    mapTypeControlOptions: {
                        mapTypeIds: ["style1"]
                    }
                }
            },
            marker: {address: "Rzeszów", options: {icon: "/themes/flatly/img/icons/marker.png"}},
            styledmaptype: {
                id: "style1",
                options: {name: "Map"},
                styles: gmapStyle
            }
        });        
    });
    
    $('.btn_gmap').click(function () {
        $('#gmap_contact').gmap3({
            clear: {name:["marker"]},
            marker: {address: $(this).attr('data-city'), options:{icon: "/themes/flatly/img/icons/marker.png"}},
            autofit:{},
            map: {options: {zoom:9}}
        });        
    });
</script>

<script>
    var gmapStyle = [{"featureType":"water","elementType":"all","stylers":[{"hue":"#7fc8ed"},{"saturation":55},{"lightness":-6},{"visibility":"on"}]},{"featureType":"water","elementType":"labels","stylers":[{"hue":"#7fc8ed"},{"saturation":55},{"lightness":-6},{"visibility":"off"}]},{"featureType":"poi.park","elementType":"geometry","stylers":[{"hue":"#83cead"},{"saturation":1},{"lightness":-15},{"visibility":"on"}]},{"featureType":"landscape","elementType":"geometry","stylers":[{"hue":"#f3f4f4"},{"saturation":-84},{"lightness":59},{"visibility":"on"}]},{"featureType":"landscape","elementType":"labels","stylers":[{"hue":"#ffffff"},{"saturation":-100},{"lightness":100},{"visibility":"off"}]},{"featureType":"road","elementType":"geometry","stylers":[{"hue":"#ffffff"},{"saturation":-100},{"lightness":100},{"visibility":"on"}]},{"featureType":"road","elementType":"labels","stylers":[{"hue":"#bbbbbb"},{"saturation":-100},{"lightness":26},{"visibility":"on"}]},{"featureType":"road.arterial","elementType":"geometry","stylers":[{"hue":"#ffcc00"},{"saturation":100},{"lightness":-35},{"visibility":"simplified"}]},{"featureType":"road.highway","elementType":"geometry","stylers":[{"hue":"#ffcc00"},{"saturation":100},{"lightness":-22},{"visibility":"on"}]},{"featureType":"poi.school","elementType":"all","stylers":[{"hue":"#d7e4e4"},{"saturation":-60},{"lightness":23},{"visibility":"on"}]}];
</script>
@endpush
