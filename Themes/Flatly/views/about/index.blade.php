@extends('layouts.master')
@section('content')
@include('partials.page-header',['page' => $page])
<div class="container">
    <div class="row row_ofirmie_top">
        <div class="col-xs-12 col_l">
            <div class="top">{!! $page->body !!}</div>
        </div>
        <div class="col-xs-12 col_r">
            <img src="/themes/flatly/img/o_firmie.jpg" alt="O firmie" class="img-responsive top_foto">
        </div>
    </div>
    <div class="row row_default">
        <div class="col-md-7 col_ofirmie_body">
            <div class="text">{!! $page->content !!}</div>
        </div>
        <div class="col-md-5 col_ofirmie_short">
            <div class="orange_line"></div>
            <div class="text">
                <div class="short">{!! $page->short !!}</div>
            </div>
            <a href="{{route('contact')}}" class="btn_orange">Napisz do nas <i class="fa fa-angle-right" aria-hidden="true"></i></a>
        </div>
    </div>
</div>
@endsection