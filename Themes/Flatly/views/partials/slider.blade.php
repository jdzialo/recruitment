@if(isset($slider))  
<div class="swiper-container">
    <div class="swiper-wrapper">        
        @if(isset($slider))        
        @foreach($slider as $slide)
        @if(isset($slide))       
        <div class="swiper-slide" style="background-image: url('{{ $slide->file()->get()->first()->path }}')">
            <div class="mask_gradient_black"></div>
            <div class="mask_pattern"></div>
            <div class="hidden-xs mask_yellow_triangle_big"></div>
            <div class="slide_text">
                @if(isset($slide->content)) 
                    {{ $slide->body }}
                @endif
            </div>
        </div>
        
        @php
            $pagination[] = ['title' => $slide->title, 'content' => $slide->content];
        @endphp
        @endif
        @endforeach;
        @endif
    </div>
    <div class="swiper-pagination"></div>
</div>
@endif

{{-- dump($pagination) --}}
{{-- dump(json_encode($pagination)) --}}
@push('style')
{!! Theme::style('vendor/swiper/dist/css/swiper.min.css') !!}
@endpush
@push('scripts')
{!! Theme::script('vendor/swiper/dist/js/swiper.min.js') !!}
<script>
    $(document).ready(function () {
        var pagination = new Array();
        pagination = {!! json_encode($pagination) !!};
        
//        var pagination = new Array();
//        pagination[0] = {'title': 'Outsourcing', 'content': 'Mauris ut pulvinar ligula. In quis ornare risus. Nullam massa aliquet, lacinia dolor eu, tempor justo.'}
//        pagination[1] = {'title': 'Pozyskiwanie klientów', 'content': 'Pulvinar ligula. In quis ornare risus. Nullam at massa aliquet, lacinia dolor eu, tempor justo.'}
//        pagination[2] = {'title': 'Umawianie spotkań', 'content': 'In quis ornare risus. Nullam at massa aliquet, lacinia dolor eu, tempor justo memorit dus.'}

        var swiper = new Swiper('.swiper-container', {
            pagination: '.swiper-pagination',
            paginationClickable: true,
            effect: 'fade',
            //loop: 'true',
            //autoplay: '2000',
            paginationBulletRender: function (swiper, index, className) {
                var title = '<div class="title">' + pagination[index].title + '</div>';
                var content = '<div class="content">' + pagination[index].content + '</div>';

                return '<div class="swiper-pagination-bullet">' + title + content + '</div>';
            }
        });
    });
</script>
@endpush