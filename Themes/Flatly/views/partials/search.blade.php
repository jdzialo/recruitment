<div class="modal fade box_search" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                Wyszukaj w aktualnościach:
                <button type="button" class="btn btn-default pull-right btn_close" data-dismiss="modal">
                    <i class="fa fa-times" aria-hidden="true"></i>
                </button>
            </div>
            <div class="modal-body">
                {!! Form::open(['route' => 'pages-lists', 'method' => 'get', 'class' => 'contactform']) !!}
                <div class="row">
                    <div class="col-md-8">
                        <div class="form-group">
                            <input type="text" name="titlesearch" class="form-control" placeholder="Wyszukaj" value="{{ old('titlesearch') }}">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <button class="btn btn_orange">Szukaj</button>
                        </div>
                    </div>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>
