<footer class="container-fluid">
    <div class="container">
        <div class="row row_top">
            <div class="hidden-xs col-sm-6 col-md-3 col_menubottom">
                <h3>Menu strony</h3>
                @menu('Menu')
            </div>
            <div class="col-sm-6 col-md-3 col_socialmedia pull-right">
                <h3>Śledź nas na bieżąco</h3>
                <p>Zapraszamy na nasze profile społecznościowe.</p>
             
              
               @if(isset($fb) && $fb != '')
                <a href="{{$fb}}" target="_blank">
                    <img src="/themes/flatly/img/icons/ico_facebook.png" alt="facebook">
                </a>
             
                
               
               @endif
             
               
                <!--
                <a href="https://twitter.com/?lang=pl" target="_blank">
                    <img src="/themes/flatly/img/icons/ico_tweeter.png" alt="twitter">
                </a>
                -->
            </div>
            <div class="col-sm-12 col-md-6 col_newsletter">
                <h3>Newsletter</h3>
                <p>Bądź na bieżąco informowany o nowych ofertach pracy. Śledź zmiany w naszej firmie. Zapisz się do newslettera już teraz!</p>
                <form class="form-horizontal frm_newsletter" method="POST" action="{{ URL::route('newsletter.subscribe.store') }}">
                   {{ csrf_field() }}
                    <div class="input-group">                     
                        <input type="email" name="email" class="form-control" placeholder="{{ trans('newsletter::emails.write_your_email') }}" required />
                        <span class="input-group-btn">
                            <button type="submit" class="btn btn-default">Zapisz się <i class="fa fa-angle-right" aria-hidden="true"></i></button>
                        </span>
                    </div>
                </form>
 
                
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12 col_gray_line">
                <div class="gray_line"></div>
            </div>
        </div>
        <div class="row row_copyright">
            <div class="col-sm-6 col-md-6 col_tradecorp">
                Wszelkie prawa zastrzeżone dla <a href="#">TradeCorp</a>
            </div>
            <div class="col-sm-6 col-md-6 col_moonbite">
                Projekt i realizacja strony internetowej: <a href="http://www.moonbite.pl">moonbite.pl</a>
            </div>
        </div>
    </div>
</footer>
