@if($items->count())
@foreach($items as $key => $item)
@if ($item->getThumbnailAttribute() != null)
@if ($loop->iteration == 3)
<div class="col-xs-12 col-sm-6 col-lg-4 hidden-sm hidden-md col_news">
@else
<div class="col-xs-12 col-sm-6 col-lg-4 col_news">
@endif
    <div class="box_home_news">
        <img src="{{ $item->getThumbnailAttribute()->path }}" class="img-responsive">
        <div class="mask_pattern"></div>
        <div class="wrapper_content">
            <div class="box_content">
                <div class="date">{{ $item->created_at->format('d-m-Y') }}</div>
                <div class="title"><a href="{{ URL::route($locale . '.blog.slug', [$item->slug]) }}">{{ $item->title }}</a> </div>
                <div class="short"><?php echo substr($item->content, 0, 100); ?>  </div>
            </div>
        </div>
    </div>
</div>
@endif  
@endforeach
@endif   

