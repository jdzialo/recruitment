<nav class="navbar navbar-default">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-menu-collapse">
                <i class="fa fa-bars" aria-hidden="true"></i> MENU
            </button>
            <a class="navbar-brand" href="{{ URL::to('/') }}">
                <img src="/assets/img/logo-trade.png" alt="logo" class="img-responsive">
            </a>
        </div>
        <div id="navbar-menu-collapse" class="navbar-collapse collapse pull-right navbar_menu">
            @menu('Menu')
            <ul class="nav navbar-nav nav_search">
                <li>
                    <button type="button" class="btn_search" data-toggle="modal" data-target="#myModal">
                        <span class="ico_search"></span> 
                        <span class="lbl_search">SZUKAJ</span>
                    </button> 
                </li>
            </ul>
        </div>
    </div>
</nav>
@include('partials.search')
@push('scripts')
{!! Theme::script('js/menu.js') !!}
@endpush