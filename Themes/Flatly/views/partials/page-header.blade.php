<div class="container-fluid container_page_header" style="background-image: url('{{ $page->getImg() }}')">
    <div class="container">
        <div class="row row_page_header">
            <div class="col-xs-12 col_page_header">
                <h1 class="page_h1">
                    <div class="title">
                        {{$page->title}}
                    </div>
                </h1>
            </div>
        </div>
    </div>
    <div class="hidden-xs mask_yellow_triangle"></div>
</div>
