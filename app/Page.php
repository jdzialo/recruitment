<?php


namespace App;


use Illuminate\Database\Eloquent\Model;

use Laravel\Scout\Searchable;


class Page extends Model

{


	use Searchable;


    public $fillable = ['template'];


    /**

     * Get the index name for the model.

     *

     * @return string

    */

    public function searchableAs()

    {

        return 'pages_index';

    }

}

