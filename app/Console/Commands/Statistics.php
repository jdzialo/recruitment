<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Modules\Dashboard\Services\StatisticsService;

class Statistics extends Command {

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'statistics';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Remove old statistics (before 5 months)';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle() {
        StatisticsService::removeOldEntities();
    }

}
