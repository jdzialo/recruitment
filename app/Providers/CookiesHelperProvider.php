<?php
/**
 * Registration of CookiesHelper as singleton app() provider
 */
namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Containers\CookiesHelper;

class CookiesHelperProvider extends ServiceProvider {

    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot() {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register() {
         $this->app->singleton('cookiesHelper', function ($app) {
            return new CookiesHelper();
        });
    }

}
