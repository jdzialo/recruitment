<?php

/**
 * Simple helper to getting cookies created by JS function in all app.
 * Helper returns a cookie value by cookieName
 * 
 * Executing helper (in all app): app()->cookiesHelper->getCookie('cookieName')
 * @author Ivan Dublianski <ivan.dublianski@gmail.com>
 * @since 2016
 */

namespace App\Containers;

class CookiesHelper {

    public function __construct() {}

    /**
     * @param string $name Name of the cookie
     * @return string|boolean
     */
    public function getCookie($name) {
        if(isset($_COOKIE[$name])){
            return $_COOKIE[$name];
        }
        return false;
    }

}
