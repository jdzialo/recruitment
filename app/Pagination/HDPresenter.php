<?php

namespace App\Pagination;

use Illuminate\Pagination\BootstrapThreePresenter;
use Illuminate\Support\HtmlString;
use Illuminate\Contracts\Pagination\Paginator as PaginatorContract;
use Illuminate\Contracts\Pagination\Presenter as PresenterContract;
use Illuminate\Pagination\UrlWindow;

class HDPresenter {
    /// use BootstrapThreeNextPreviousButtonRendererTrait, UrlWindowPresenterTrait;

    /**
     * The paginator implementation.
     *
     * @var \Illuminate\Contracts\Pagination\Paginator
     */
    protected $paginator;

    /**
     * The URL window data structure.
     *
     * @var array
     */
    protected $window;

    /**
     * Create a new Bootstrap presenter instance.
     *
     * @param  \Illuminate\Contracts\Pagination\Paginator  $paginator
     * @param  \Illuminate\Pagination\UrlWindow|null  $window
     * @return void
     */
    public function __construct(PaginatorContract $paginator, UrlWindow $window = null) {

        ;
        $this->paginator = $paginator;
        $this->window = is_null($window) ? UrlWindow::make($paginator) : $window->get();
    }

    /**
     * Determine if the underlying paginator being presented has pages to show.
     *
     * @return bool
     */
    public function hasPages() {
        if (!empty($this->paginator->items() != null && $this->paginator->items())) {
            return true;
        }
        return false;
    }

    /**
     * Get HTML wrapper for an available page link.
     *
     * @param  string  $url
     * @param  int  $page
     * @param  string|null  $rel
     * @return string
     */
    protected function getAvailablePageWrapper($url, $page, $rel = null) {
        $rel = is_null($rel) ? '' : ' rel="' . $rel . '"';

        return '<li><a href="' . htmlentities($url) . '"' . $rel . '>' . $page . '</a></li>';
    }

    /**
     * Get HTML wrapper for disabled text.
     *
     * @param  string  $text
     * @return string
     */
    protected function getDisabledTextWrapper($text) {
        return '<li class="disabled"><span>' . $text . '</span></li>';
    }

    /**
     * Get HTML wrapper for active text.
     *
     * @param  string  $text
     * @return string
     */
    protected function getActivePageWrapper($text) {
        return '<li class="active"><span>' . $text . '</span></li>';
    }

    /**
     * Get a pagination "dot" element.
     *
     * @return string
     */
    protected function getDots() {
        return $this->getDisabledTextWrapper('...');
    }

    /**
     * Get the current page from the paginator.
     *
     * @return int
     */
    protected function currentPage() {
        return $this->paginator->currentPage();
    }

    /**
     * Get the last page from the paginator.
     *
     * @return int
     */
    protected function lastPage() {
        return $this->paginator->lastPage();
    }

    public function render() {

        if ($this->hasPages()) {

            return sprintf(
                    '<div class="pagi-custom"><div class="pull-left">%s %s</div> %s <div class="pull-right">%s %s</div></div>', $this->getFirst(), $this->getButtonPre(), $this->getButtonCurrent(), $this->getButtonNext(), $this->getLast()
            );
        }
        return "";
    }

    public function getLast() {

        $url = $this->paginator->url($this->paginator->lastPage());
        $btnStatus = '';

        if ($this->paginator->lastPage() == $this->paginator->currentPage()) {
            $btnStatus = 'disabled';
        }
        return $btn = "<a href='" . $url . "'><button class='btn btn-success " . $btnStatus . "'> <i class='fa fa-angle-right' aria-hidden='true'></i><i class='fa fa-angle-right' aria-hidden='true'></i></button></a>";
    }

    public function getFirst() {
        $url = $this->paginator->url(1);
        $btnStatus = '';

        if (1 == $this->paginator->currentPage()) {
            $btnStatus = 'disabled';
        }
        return $btn = "<a href='" . $url . "'><button class='btn btn-success " . $btnStatus . "'><i class='fa fa-angle-left' aria-hidden='true'></i><i class='fa fa-angle-left' aria-hidden='true'></i> </button></a>";
    }

    public function getButtonPre() {
        $url = $this->paginator->previousPageUrl();
        $btnStatus = '';

        if (empty($url)) {
            $btnStatus = 'disabled';
        }
        return $btn = "<a href='" . $url . "'><button class='btn btn-success " . $btnStatus . "'><i class='fa fa-angle-left' aria-hidden='true'></i></button></a>";
    }

    public function getButtonCurrent() {
        $url = $this->paginator->currentPage();
        $btnStatus = '';

        if (empty($url)) {
            $btnStatus = 'disabled';
        }
        return $btn = "<a href='" . $url . "'><button class='btn btn-success " . $btnStatus . "'>" . $url . "</button></a>";
    }

    public function getButtonNext() {
        $url = $this->paginator->nextPageUrl();
        $btnStatus = '';

        if (empty($url)) {
            $btnStatus = 'disabled';
        }
        return $btn = "<a href='" . $url . "'><button class='btn btn-success " . $btnStatus . "'> <i class='fa fa-angle-right' aria-hidden='true'></i></button></a>";
    }

}
